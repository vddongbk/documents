const mongoose = require('mongoose');
const util = require('util');

// config should be imported before importing any other file
const config = require('./config/config');
const app = require('./config/express');

const debug = require('debug')(config.debug);

// connect mongodb
require('./config/database');

// print mongoose logs in dev env
if (config.mongooseDebug) {
  mongoose.set('debug', (collectionName, method, query, doc) => {
    debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
  });
}

// module.parent check is required to support mocha watch
// src: https://github.com/mochajs/mocha/issues/1912
if (!module.parent) {
  // eslint-disable-next-line
  _Roller.roll()
    .then(() => {
      // start server
      // listen on port config.port
      app.listen(config.port, (err) => {
        if (err) {
          debug('Start server error');
        } else {
          // eslint-disable-next-line
          console.info(
            `
              =====================================================
              -> Server 🏃 (running) on Port:${config.port} (${config.env})
              =====================================================
            `
          );
        }
      });
    });
}

module.exports = app;
