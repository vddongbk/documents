### MongoDb Transaction

## Implement

```js
const transaction = require('./transaction');
```

## Usage

```js
transaction.db().save(User, { email: 'vddongbk5@gmail.com', password: '123123' });
    transaction.db().update(User, { _id: 'qweqwe' }, { email: 'test@gmail.com' });

    // eslint-disable-next-line
    await transaction.db().run({ useMongoose: true })
      .then(() => {
        // update is complete
      })
      .catch((err) => {
        // Everything has been rolled back.

        // log the error which caused the failure
        throw err;
      });
```

## Files

- app.js (Http express app)
- database (Config db)
- transaction (Global transaction)
- README.md (note)

## Modules

- [fawn](https://www.npmjs.com/package/fawn)

## References
