const mongoose = require('mongoose');
const config = require('./config');
const Fawn = require('fawn');

// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign

// plugin bluebird promise in mongoose
mongoose.Promise = Promise;

// connect to mongo db
const mongoUri = config.mongo.host || 'mongodb://localhost/express-mongoose-es6-rest-api';
mongoose.connect(mongoUri, {});
// listen for connection errors and print the message
mongoose.connection.on('error', (err) => {
  // eslint-disable-next-line
  console.error(`⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨  → ${err.message}`);
  throw err;
});

Fawn.init(mongoose);

// eslint-disable-next-line
global._DB = Fawn.Task();
// eslint-disable-next-line
global._Roller = Fawn.Roller();
