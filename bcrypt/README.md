### Bcrypt Service

## Implement

```js
const BcryptService = require('./BcryptService.js');
```

## Usage

*Hash*

```js
const passwordHash = await BcryptService.hash(password);
// return string hash
```

*compare*

```js
let isPasswordCorrect = await BcryptService.compare(password, passwordHash);
// return true or false
```

## Files

- BcryptService.js (Bcrypt Service)
- README.md (note)

## Modules

- [bcrypt-nodejs](https://www.npmjs.com/package/bcrypt-nodejs)

## References
