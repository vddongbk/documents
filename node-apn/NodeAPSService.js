var apn = require('apn');

var configOptions = require('./config.js');

/**
 *
 * @param options
 * https://github.com/node-apn/node-apn/blob/master/doc/notification.markdown
 * @param deviceToken
 * @returns {Promise<any>}
 * @constructor
 */
const Notification = (options = {}, deviceToken) => {
  return new Promise((resolve, reject) => {
      const apnProvider = new apn.Provider(configOptions);
      const note = new apn.Notification(options);

      apnProvider.send(note, deviceToken).then(result => {
          resolve(result);
      }).catch(err => {
          reject(err)
      });

      apnProvider.shutdown();
  })
};

module.exports = Notification;
