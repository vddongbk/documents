### NODE APN (IOS Notifications)

## Implement

```js
const notifyIOS = require('./NodeAPSService.js');
```

## Usage

```js
notifyIOS({
    alert: "Hello, world!",
    sound: "chime.caf",
    mutableContent: 1,
    payload: {
        "sender": "node-apn",
    }
}, deviceToken);
```

## Files

- NodeAPSService.js (Node APS Service)
- config.js (Config Node APS Service)
- certificates/cert.pem (Certificates Key)
- certificates/key.pem (Private Key)
- config.js (Config Node APS Service)
- README.md (note)

## Modules

- [apn](https://www.npmjs.com/package/apn)

## References
