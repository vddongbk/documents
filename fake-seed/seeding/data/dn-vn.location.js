module.exports = [
  {
    latitude: 16.041900,
    longitude: 108.183920
  },
  {
    latitude: 16.074013,
    longitude: 108.206524
  },
  {
    latitude: 16.074835,
    longitude: 108.189894
  },
  {
    latitude: 16.064330,
    longitude: 108.207704
  },
  {
    latitude: 16.050498,
    longitude: 108.194081
  },
  {
    latitude: 16.056429,
    longitude: 108.201114
  },
  {
    latitude: 16.030483,
    longitude: 108.204990
  },
  {
    latitude: 16.054945,
    longitude: 108.217957
  },
  {
    latitude: 16.041583,
    longitude: 108.193943
  },
  {
    latitude: 16.024928,
    longitude: 108.196876
  },
  {
    latitude: 16.050787,
    longitude: 108.193734
  },
  {
    latitude: 16.036865,
    longitude: 108.212390
  },
  {
    latitude: 16.046171,
    longitude: 108.189010
  },
  {
    latitude: 16.069162,
    longitude: 108.213479
  },
  {
    latitude: 16.074506,
    longitude: 108.196729
  },
  {
    latitude: 16.061676,
    longitude: 108.208567
  },
  {
    latitude: 16.030157,
    longitude: 108.186615
  },
  {
    latitude: 16.050335,
    longitude: 108.206919
  },
  {
    latitude: 16.069630,
    longitude: 108.182889
  },
  {
    latitude: 16.061213,
    longitude: 108.202333
  },
  {
    latitude: 16.039849,
    longitude: 108.206761
  },
  {
    latitude: 16.033507,
    longitude: 108.220511
  },
  {
    latitude: 16.036657,
    longitude: 108.184224
  },
  {
    latitude: 16.042136,
    longitude: 108.222105
  },
  {
    latitude: 16.050150,
    longitude: 108.208532
  },
  {
    latitude: 16.040832,
    longitude: 108.209729
  },
  {
    latitude: 16.063686,
    longitude: 108.209069
  },
  {
    latitude: 16.065569,
    longitude: 108.202754
  },
  {
    latitude: 16.037107,
    longitude: 108.210053
  },
  {
    latitude: 16.074762,
    longitude: 108.202425
  },
  {
    latitude: 16.074796,
    longitude: 108.201252
  },
  {
    latitude: 16.023955,
    longitude: 108.194929
  },
  {
    latitude: 16.068928,
    longitude: 108.190354
  },
  {
    latitude: 16.048706,
    longitude: 108.219285
  },
  {
    latitude: 16.042839,
    longitude: 108.183187
  },
  {
    latitude: 16.059084,
    longitude: 108.210981
  },
  {
    latitude: 16.059028,
    longitude: 108.205792
  },
  {
    latitude: 16.067139,
    longitude: 108.220157
  },
  {
    latitude: 16.071403,
    longitude: 108.219650
  },
  {
    latitude: 16.051215,
    longitude: 108.217655
  },
  {
    latitude: 16.032284,
    longitude: 108.194273
  },
  {
    latitude: 16.056529,
    longitude: 108.194127
  },
  {
    latitude: 16.024641,
    longitude: 108.205925
  },
  {
    latitude: 16.035182,
    longitude: 108.220524
  },
  {
    latitude: 16.022063,
    longitude: 108.198311
  },
  {
    latitude: 16.074692,
    longitude: 108.189647
  },
  {
    latitude: 16.039791,
    longitude: 108.192456
  },
  {
    latitude: 16.032482,
    longitude: 108.223232
  },
  {
    latitude: 16.049734,
    longitude: 108.220712
  },
  {
    latitude: 16.038557,
    longitude: 108.192919
  },
  {
    latitude: 16.047514,
    longitude: 108.222842
  },
  {
    latitude: 16.066029,
    longitude: 108.218596
  },
  {
    latitude: 16.023569,
    longitude: 108.203603
  },
  {
    latitude: 16.040956,
    longitude: 108.195524
  },
  {
    latitude: 16.052790,
    longitude: 108.195395
  },
  {
    latitude: 16.063909,
    longitude: 108.210961
  },
  {
    latitude: 16.038231,
    longitude: 108.190717
  },
  {
    latitude: 16.059834,
    longitude: 108.182692
  },
  {
    latitude: 16.050665,
    longitude: 108.214336
  },
  {
    latitude: 16.048231,
    longitude: 108.220925
  },
  {
    latitude: 16.066380,
    longitude: 108.197971
  },
  {
    latitude: 16.044242,
    longitude: 108.204930
  },
  {
    latitude: 16.051444,
    longitude: 108.187283
  },
  {
    latitude: 16.043661,
    longitude: 108.197484
  },
  {
    latitude: 16.057122,
    longitude: 108.201055
  },
  {
    latitude: 16.071932,
    longitude: 108.183398
  },
  {
    latitude: 16.032234,
    longitude: 108.205037
  },
  {
    latitude: 16.046348,
    longitude: 108.202377
  },
  {
    latitude: 16.040189,
    longitude: 108.190889
  },
  {
    latitude: 16.034154,
    longitude: 108.221478
  },
  {
    latitude: 16.047630,
    longitude: 108.213625
  },
  {
    latitude: 16.021611,
    longitude: 108.222485
  },
  {
    latitude: 16.072082,
    longitude: 108.219777
  },
  {
    latitude: 16.052500,
    longitude: 108.183946
  },
  {
    latitude: 16.046998,
    longitude: 108.220926
  },
  {
    latitude: 16.053755,
    longitude: 108.191243
  },
  {
    latitude: 16.030007,
    longitude: 108.219711
  },
  {
    latitude: 16.068095,
    longitude: 108.209079
  },
  {
    latitude: 16.043455,
    longitude: 108.215336
  },
  {
    latitude: 16.057280,
    longitude: 108.208927
  },
  {
    latitude: 16.054539,
    longitude: 108.201305
  },
  {
    latitude: 16.068229,
    longitude: 108.220832
  },
  {
    latitude: 16.062458,
    longitude: 108.193993
  },
  {
    latitude: 16.035015,
    longitude: 108.195912
  },
  {
    latitude: 16.043169,
    longitude: 108.207956
  },
  {
    latitude: 16.072731,
    longitude: 108.217215
  },
  {
    latitude: 16.062509,
    longitude: 108.183912
  },
  {
    latitude: 16.049113,
    longitude: 108.209450
  },
  {
    latitude: 16.062148,
    longitude: 108.216275
  },
  {
    latitude: 16.045371,
    longitude: 108.202714
  },
  {
    latitude: 16.050631,
    longitude: 108.219870
  },
  {
    latitude: 16.024950,
    longitude: 108.206490
  },
  {
    latitude: 16.055411,
    longitude: 108.214607
  },
  {
    latitude: 16.043827,
    longitude: 108.204120
  },
  {
    latitude: 16.074761,
    longitude: 108.182821
  },
  {
    latitude: 16.053822,
    longitude: 108.198562
  },
  {
    latitude: 16.072882,
    longitude: 108.215655
  },
  {
    latitude: 16.071975,
    longitude: 108.203372
  },
  {
    latitude: 16.040478,
    longitude: 108.220756
  },
  {
    latitude: 16.074180,
    longitude: 108.191162
  }
];
