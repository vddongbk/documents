const faker = require('faker');
const Path = require('path');
const uid = require('uid');
const moment = require('moment-timezone');

moment.tz.setDefault('Asia/Ho_Chi_Minh');

// const fakergem = require('fakergem');
// import  data
const dataUserTest = require('./data/user-test.seed');
const dataPayment = require('./data/payment.seed');
const dataType = require('./data/taxi-type.seed');

// import models
const {
  User,
  Company,
  TaxiType,
  Taxi,
  Payment,
  Review,
  Trip,
} = require(Path.resolve(__dirname, '..', 'api/models'));
/**
 *
 * @param name
 * @returns {*}
 */
function randomImage(name) {
  switch (name) {
    case 'avatar':
      return faker.image.avatar();
    case 'food':
      return faker.image.food();
    case 'business':
      return faker.image.business();
    case 'transport':
      return faker.image.transport();
    default:
      return faker.image.fashion();
  }
}

/**
 *
 * @param name
 * @returns {Array}
 */
function getArrayImages(name) {
  const images = [];
  const number = faker.random.number({ min: 1, max: 10 });
  for (let i = 0; i < number; i += 1) {
    images.push({
      origin: randomImage(name),
      thumb: randomImage(name)
    });
  }
  return images;
}

/**
 * Create Faker Admin
 */
async function fakerUserTest() {
  try {
    await User.create(dataUserTest);
    return true;
  } catch (error) {
    throw error;
  }
}

async function fakerCompany() {
  try {
    const promises = [];
    for (let i = 0; i < 10; i += 1) {
      const data = {
        logo: randomImage('business'),
        name: faker.company.companyName(),
        address: faker.address.streetAddress(),
      };
      promises.push(Company.create(data));
    }
    await Promise.all(promises);
    return true;
  } catch (error) {
    throw error;
  }
}

async function fakerPayment() {
  try {
    await Payment.create(dataPayment);
    return true;
  } catch (error) {
    throw error;
  }
}

function getArrayIds(dataInput) {
  const dataOutput = [];
  for (let i = 0; i < dataInput.length; i += 1) {
    dataOutput.push(dataInput[i]._id);
  }
  return dataOutput;
}

async function fakerTaxiType() {
  try {
    const companies = await Company.find();
    const promises = [];
    for (let i = 0; i < dataType.length; i += 1) {
      const company = getArrayIds(companies);
      const data = {
        name: dataType[i].name,
        company,
      };
      promises.push(TaxiType.create(data));
    }
    await Promise.all(promises);
    return true;
  } catch (error) {
    throw error;
  }
}

async function fakerUsers() {
  try {
    const promises = [];
    for (let i = 0; i < 200; i += 1) {
      const data = {
        email: faker.internet.email().toLowerCase(),
        password: '123123',
        username: faker.internet.userName(),
        gender: faker.random.number({ min: 1, max: 3 }),
        birthday: new Date(),
        phone: faker.phone.phoneNumber(),
        isVerified: true,
        isBlocked: true,
        invitationCode: uid(6),
        countryCode: faker.address.countryCode(),
        isDriver: faker.random.boolean(),
        // taxi: {
        //   type: Schema.Types.ObjectId,
        //   ref: 'Taxi'
        // },
        companyReview: faker.lorem.sentence(),
        loginAt: new Date(),
        passwordUpdatedAt: new Date(),
        // argRating: {
        //   type: Number
        // },
      };
      promises.push(User.create(data));
    }
    await Promise.all(promises);
    return true;
  } catch (error) {
    throw error;
  }
}

async function fakerTaxi() {
  try {
    const startData = await Promise.all([
      User.find({ isDriver: true }),
      TaxiType.find({}),
      Company.find({})
    ]);
    let promises = [];
    for (let i = 0; i < startData[0].length; i += 1) {
      const data = {
        identifier: uid(8),
        image: randomImage('transport'),
        user: startData[0][i],
        // number: {
        //   type: Number,
        // },
        company: startData[2][faker.random.number(startData[2].length - 1)]._id,
        type: startData[1][faker.random.number(startData[1].length - 1)]._id,
        memo: faker.lorem.sentence(),
        active: 3,
        state: 2,
      };
      promises.push(Taxi.create(data));
    }
    await Promise.all(promises);

    promises = [];

    const taxi = await Taxi.find({});
    for (let i = 0; i < taxi.length; i += 1) {
      // eslint-disable-next-line
      promises.push(User.findOneAndUpdate({ _id: taxi[i].user }, { $set: { taxi: taxi[i]._id } }, { new: true }));
    }
    await Promise.all(promises);
    return true;
  } catch (error) {
    throw error;
  }
}

async function fakerTrip() {
  try {
    const startData = await Promise.all([
      User.find({ isDriver: false }),
      User.find({ isDriver: true }),
      Payment.find({}),
    ]);
    const promises = [];
    for (let i = 0; i < 100; i += 1) {
      const driver = startData[1][faker.random.number(startData[1].length - 1)]._id;
      const taxi = await Taxi.findOne({ user: driver }).populate('company').select('_id');
      const taxiType = await TaxiType.find({ company: taxi.company._id });
      const data = {
        toLocation: {
          type: 'Point',
          coordinates: [faker.address.longitude, faker.address.latitude],
        },
        fromLocation: {
          type: 'Point',
          coordinates: [faker.address.longitude, faker.address.latitude],
        },
        typeBooking: 1,
        user: startData[0][faker.random.number(startData[0].length - 1)]._id,
        typeTaxi: taxiType[faker.random.number(taxiType.length - 1)]._id,
        driver,
        payment: startData[2][faker.random.number(startData[2].length - 1)]._id,
        cost: faker.random.number({ min: 10000, max: 10000000 }),
        distance: faker.random.number({ min: 0, max: 1000000 }),
        state: faker.random.number({ min: 1, max: 4 }),
        startTime: moment(new Date()).subtract(4, 'hours'),
        endTime: moment(new Date()).subtract(2, 'hours')
      };
      promises.push(Trip.create(data));
    }
    await Promise.all(promises);
    return true;
  } catch (error) {
    throw error;
  }
}

async function fakerReview() {
  try {
    const startData = await Promise.all([
      Trip.find({ state: 4 }),
    ]);
    for (let i = 0; i < startData[0].length; i += 1) {
      const data = {
        user: startData[0][i].user,
        trip: startData[0][i]._id,
        rate: faker.random.number({ min: 1, max: 5 }),
        comment: faker.lorem.sentence(),
      };
      await Review.create(data);
      const driver = await User.findOne({ _id: startData[0][i].driver });
      const argRating = driver.argRating || 0;
      await User.findOneAndUpdate({
        _id: startData[0][i].driver
      }, {
        $set: {
          argRating: (argRating + data.rate) / 2
        }
      }, { new: true });
    }
    return true;
  } catch (error) {
    throw error;
  }
}

async function removeUsers() {
  try {
    await User.remove();
  } catch (error) {
    throw error;
  }
}

async function removeCompany() {
  try {
    await Company.remove();
  } catch (error) {
    throw error;
  }
}

async function removeTaxiType() {
  try {
    await TaxiType.remove();
  } catch (error) {
    throw error;
  }
}

async function removeTaxi() {
  try {
    await Taxi.remove();
  } catch (error) {
    throw error;
  }
}

async function removePayment() {
  try {
    await Payment.remove();
  } catch (error) {
    throw error;
  }
}

async function removeReview() {
  try {
    await Review.remove();
  } catch (error) {
    throw error;
  }
}

async function removeTrip() {
  try {
    await Trip.remove();
  } catch (error) {
    throw error;
  }
}

module.exports = {
  randomImage,
  getArrayImages,
  fakerUserTest,
  removeUsers,
  removeCompany,
  removeTaxiType,
  removeTaxi,
  removePayment,
  removeReview,
  removeTrip,
  fakerCompany,
  fakerPayment,
  fakerTaxiType,
  fakerUsers,
  fakerTaxi,
  fakerTrip,
  fakerReview,
};
