/* eslint no-param-reassign: ["error", { "props": false }]*/
const _ = require('lodash');
const Listr = require('listr');
const moment = require('moment-timezone');
require('./../config/database');
require('./../config/global');

moment.tz.setDefault('Asia/Ho_Chi_Minh');

const seed = require('./helpers');

/**
 * Load data into MongoDB
 *
 * This method deletes existing data before importing the
 * samples. Be careful here, there’s no approval question
 * before deletion.
 *
 * @return {Array} tasks for listr
 */
function pumpItUp() {
  return _.concat(
    // add task to remove data before import to avoid errors
    destroyDB(),

    // the actual tasks to import data
    [
      {
        title: 'Create user test simple for user model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing user test';
          await seed.fakerUserTest();
        }
      },
      {
        title: 'Create company simple for company model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing company';
          await seed.fakerCompany();
        }
      },
      {
        title: 'Create taxi type simple for taxi type model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing taxi type';
          await seed.fakerTaxiType();
        }
      },
      {
        title: 'Create payment simple for payment model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing payment';
          await seed.fakerPayment();
        }
      },
      {
        title: 'Create user simple for user model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing user';
          await seed.fakerUsers();
        }
      },
      {
        title: 'Create taxi simple for taxi model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing taxi';
          await seed.fakerTaxi();
        }
      },
      {
        title: 'Create trip simple for trip model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing trip';
          await seed.fakerTrip();
        }
      },
      {
        title: 'Create review simple for review model 👌',
        task: async (ctx, task) => {
          task.output = 'Importing review';
          await seed.fakerReview();
        }
      },
    ]
  );
}

/**
 * Delete all data from MongoDB
 *
 * @return {Array} tasks for listr
 */
function destroyDB() {
  return [
    {
      title: 'Remove user collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove user collection';
        await seed.removeUsers();
      }
    },
    {
      title: 'Remove company collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove company collection';
        await seed.removeCompany();
      }
    },
    {
      title: 'Remove taxiType collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove taxiType collection';
        await seed.removeTaxiType();
      }
    },
    {
      title: 'Remove taxi collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove taxi collection';
        await seed.removeTaxi();
      }
    },
    {
      title: 'Remove payment collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove payment collection';
        await seed.removePayment();
      }
    },
    {
      title: 'Remove review collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove review collection';
        await seed.removeReview();
      }
    },
    {
      title: 'Remove trip collection 👌',
      task: async (ctx, task) => {
        task.output = 'Remove trip collection';
        await seed.removeTrip();
      }
    },
  ];
}

/**
 * Start tasks to prepare or destroy data in MongoDB
 *
 * @param  {Listr} tasks  Listr instance with tasks
 * @return {void}
 */
async function kickoff(tasks) {
  await tasks.run();
  process.exit();
}

/**
 * Entry point for the NPM "pumpitup" and "cleanup" scripts
 */

if (process.argv.includes('--destroy')) {
  const cleanUp = destroyDB();
  kickoff(new Listr(cleanUp));
} else {
  const pumpIt = pumpItUp();
  kickoff(new Listr(pumpIt));
}
