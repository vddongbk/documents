### Fake - Seed Data

## Implement

```js
- package.json
 "scripts": {
    "pumpitup": "node ./seeding/index.js"
 }
```

## Usage


`npm run pumpitup`

## Files

- seeding/ (config seed)
- README.md (note)

## Modules

- [lodash](https://www.npmjs.com/package/lodash)
- [listr](https://www.npmjs.com/package/listr)
- [moment-timezone](https://www.npmjs.com/package/moment-timezone)
- [faker](https://www.npmjs.com/package/faker)
- [path](https://www.npmjs.com/package/path)
- [uid](https://www.npmjs.com/package/uid)
- [fakergem](https://www.npmjs.com/package/fakergem)

## References
