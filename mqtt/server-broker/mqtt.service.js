const mqtt = require('mqtt');
const assert = require('assert');

const client = mqtt.connect(process.env.MQTT_HOST);

let connected = false;
client.on('connect', () => {
  connected = client.connected;
});

module.exports = {
  client,
  connected,
  /**
   *
   * @param topic
   * @param options
   * {
   *  qos: 0,1,2
   * }
   * @returns {Promise<any>}
   */
  subscribe: (topic, options = {}) => {
    assert(topic, 'MqttService - "topic" is required');
    const promise = new Promise((resolve, reject) => {
      if (!connected) reject(new Error('Cannot connect'));
      client.subscribe(topic, options, (err, granted) => {
        if (err) reject(err);
        resolve(granted);
      });

      client.on('error', (error) => {
        reject(error);
      });

      client.on('close', () => {
        reject(new Error('Cannot connect'));
      });

      // setTimeout(() => {
      //   client.end();
      //   reject(new Error('Cannot connect'));
      // }, 5000);
    });
    return promise;
  },
  /**
   *
   * @param topic
   * @returns {Promise<any>}
   */
  unsubscribe: (topic) => {
    assert(topic, 'MqttService - "topic" is required');
    const promise = new Promise((resolve, reject) => {
      if (!connected) reject(new Error('Cannot connect'));
      client.unsubscribe(topic, (err) => {
        if (err) reject(err);
        resolve();
      });

      client.on('error', (error) => {
        reject(error);
      });

      client.on('close', () => {
        reject(new Error('Cannot connect'));
      });

      // setTimeout(() => {
      //   reject('Cannot connect');
      // }, 5000);
    });
    return promise;
  },
  /**
   *
   * @param topic
   * @param message
   * @param options
   * {
   *  qos: enum[0,1,2] default: 0
   *  retain: boolean default: false
   *  dup: boolean default: false
   * }
   * @returns {Promise<any>}
   */
  publish: (topic, message, options = {}) => {
    assert(topic, 'MqttService - "topic" is required');
    assert(message, 'MqttService - "message" is required');
    const promise = new Promise((resolve, reject) => {
      if (!connected) reject(new Error('Cannot connect'));
      client.publish(topic, message, options, (err) => {
        if (err) reject(err);
        resolve();
      });

      client.on('error', (error) => {
        reject(error);
      });

      client.on('close', () => {
        reject(new Error('Cannot connect'));
      });

      // setTimeout(() => {
      //   reject('Cannot connect');
      // }, 5000);
    });
    return promise;
  },
};
