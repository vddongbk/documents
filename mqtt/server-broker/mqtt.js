const mqtt = require('mqtt');
const config = require('./config');
const debug = require('./debug');
const handle = require('./mqtt/handle');

const client = mqtt.connect(config.mqtt.host);

/** subscribe to a topic */
/*
  subscribe method takes two arguments {topic,options}.
 */
const subscribe = (topic) => {
  client.subscribe(topic, { qos: 1 }, (err, granted) => {
    if (err) subscribe(topic);
    debug('D2D API subscribed D2D Broker : ', granted);
  });
};

/** client on connect **/
client.on('connect', () => {
  debug('D2D API is connected to D2D broker');
  subscribe('taxi/tracking');
});

/** client on reconnect **/
client.on('reconnect', () => {
  debug('D2D API is reconnected');
});

/** client on error ***/
client.on('error', (err) => {
  debug('error from client --> ', err);
});

/** client on close **/
client.on('close', () => {
  debug('D2D API is closed');
});

/** client on offline **/
client.on('offline', (err) => {
  debug('D2D API is offline', err);
});

/** listen for a message **/
client.on('message', (topic, message) => {
  handle(topic, message);
});
