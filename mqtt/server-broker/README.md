### MQTT SERVER/BROKER

> *Modules*

[mqtt](https://www.npmjs.com/package/mqtt)
[assert](https://www.npmjs.com/package/assert)
[kue](https://www.npmjs.com/package/kue)
    
> *Setup*

    `npm i mqtt --save`
    
> *Usage*

*implement*

```js
 const MqttService = require('mqtt.service');
```

*Subscribe*

```js
 const result = await MqttService.subscribe(topic, { qos: 1 });
```


*Publish*

```js
 const result = await MqttService.publish(topic, message, { qos: 1 });
```


*Qos (Qualities of service)*
type: 0, 1, 2.

[Document](https://kipalog.com/posts/Tim-hieu-ve-giao-thuc-MQTT---IoT-protocol)

## Files

- mqtt.service.js (MQTT Service)
- .env (Environment variables)
- README.md (note)
- tracking.job.js (queue job tracking)
- mqtt.js (server connect and handle)
- mqtt/handle.js (handle main)

> *References*

- https://kipalog.com/posts/Tim-hieu-ve-giao-thuc-MQTT---IoT-protocol
- https://github.com/Introvertuous/react_native_mqtt/blob/master/src/mqttws31.js
- https://www.npmjs.com/package/mqtt   
