const { trackingJob } = require('../jobs');

module.exports = async (topic, message) => {
  switch (topic) {
    case 'taxi/tracking': {
      const data = JSON.parse(message.toString());
      const { driverId, latitude, longitude } = data;
      trackingJob({ title: 'Taxi tracking', driverId, latitude, longitude });
      break;
    }
    default:
  }
};
