### JWT (Json web token) Service

## Implement

```js
const JWTService = require('./JWTService.js');
```

## Usage

```js
const token = await JWTService.generateToken(data, secret, expire);
const decode = await JWTService.jwtVerify(data, secret);
// secret and expire default value
```

## Files

- JWTService.js (JWT Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)

## References
