"use strict";

const jwt = require("jsonwebtoken");

const JwtService = {

    /**
     *
     * @param data
     * @return token
     */
    generateToken: (data, secret = process.env.JWT_SECRET, expire = process.env.JWT_EXPIRE_TIME) => {
        return jwt.sign(data, secret, {expiresIn: expire});
    },

    /**
     * Verify token and return userId from token if token correct
     * @param token
     * @return {Promise}
     */
    jwtVerify: (token, secret = process.env.JWT_SECRET) => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, secret, (error, decode) => {
                if (error) return reject(error);
                resolve(decode);
            });
        })
    }
};

module.exports = JwtService;
