const MqttService = require('../services/mqtt.service');
const kue = require('kue');
const debug = require('../../config/debug');

const mqttPublish = (data) => {
  // eslint-disable-next-line
  const job = _queue.create('publish', data)
    .priority('high')
    .attempts(5)
    .backoff(true)
    .removeOnComplete(true)
    .on('start', () => {
      debug('Job publish', job.id, 'to topic', job.data.topic, 'is start');
    })
    .on('progress', () => {
      debug('Job publish', job.id, 'to topic', job.data.topic, 'is progress');
    })
    .on('remove', () => {
      debug('Job publish', job.id, 'to topic', job.data.topic, 'is remove');
    })
    .on('complete', () => {
      debug('Job publish', job.id, 'to topic', job.data.topic, 'is done');
    })
    .on('failed', () => {
      debug('Job publish', job.id, 'to topic', job.data.topic, 'has failed');
    })
    .save((err) => {
      if (err) {
        debug(`create job err: ${job.id}`);
      }
    });

  // eslint-disable-next-line
  _queue.process('publish', async (jobProcess, done) => {
    try {
      const topic = jobProcess.data.topic;
      const message = jobProcess.data.message;
      const result = await MqttService.publish(topic, message, { qos: 1 });
      debug(result);
      return done && done();
    } catch (error) {
      debug(error);
      return done && done(error);
    }
  });

  // others are active, complete, failed, delayed
  // you may want to fetch each id to get the Job object out of it...
  // eslint-disable-next-line
  _queue.failed((err, ids) => {
    ids.forEach((id) => {
      // eslint-disable-next-line
      kue.Job.get(id, (err, job) => {
        job.inactive();
      });
    });
  });
};
module.exports = mqttPublish;
