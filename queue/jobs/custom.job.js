const kue = require('kue');
const debug = require('../../config/debug');

const customJob = (data, nameJob, handle) => {
  // eslint-disable-next-line
  const job = _queue.create(nameJob, data)
    .priority('high')
    .attempts(5)
    .backoff(true)
    .removeOnComplete(true)
    .on('start', () => {
      debug('Job', job.id, 'is start');
    })
    .on('progress', () => {
      debug('Job', job.id, 'is progress');
    })
    .on('remove', () => {
      debug('Job', job.id, 'is remove');
    })
    .on('complete', () => {
      debug('Job', job.id, 'is done');
    })
    .on('failed', () => {
      debug('Job', job.id, 'has failed');
      kue.Job.get(job.id, (err, findjob) => {
        if (err) return;
        findjob.remove();
      });
    })
    .save((err) => {
      if (err) {
        debug(`create job err: ${job.id}`);
      }
    });

  // eslint-disable-next-line
  _queue.process(nameJob, async (jobProcess, done) => {
    try {
      await handle(jobProcess, done);
      // return done && done();
    } catch (error) {
      debug(error);
      return done && done(error);
    }
  });

  // others are active, complete, failed, delayed
  // you may want to fetch each id to get the Job object out of it...
  // eslint-disable-next-line
  // _queue.failed((err, ids) => {
  //   ids.forEach((id) => {
  //     // eslint-disable-next-line
  //     kue.Job.get(id, (err, job) => {
  //       job.inactive();
  //     });
  //   });
  // });
};
module.exports = customJob;
