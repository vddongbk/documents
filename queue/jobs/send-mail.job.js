const EmailService = require('../services/email.service');
const kue = require('kue');

const sendMail = (data) => {
  // eslint-disable-next-line
  const job = _queue.create('email', data)
    .priority('high')
    .attempts(5)
    .backoff(true)
    .removeOnComplete(true)
    .on('start', () => {
      // eslint-disable-next-line
      _debug('Job', job.id, 'of type', job.data.type, 'is start');
    })
    .on('progress', () => {
      // eslint-disable-next-line
      _debug('Job', job.id, 'of type', job.data.type, 'is progress');
    })
    .on('remove', () => {
      // eslint-disable-next-line
      _debug('Job', job.id, 'of type', job.data.type, 'is remove');
    })
    .on('complete', () => {
      // eslint-disable-next-line
      _debug('Job', job.id, 'of type', job.data.type, 'is done');
    })
    .on('failed', () => {
      // eslint-disable-next-line
      _debug('Job', job.id, 'of type', job.data.type, 'has failed');
    })
    .save((err) => {
      if (err) {
        // eslint-disable-next-line
        _debug(`create job err: ${job.id}`);
      }
    });

  // eslint-disable-next-line
  _queue.process('email', async (jobProcess, done) => {
    try {
      const type = jobProcess.data.type || 'example';
      await EmailService[type](jobProcess.data);
      return done && done();
    } catch (error) {
      // eslint-disable-next-line
      _debug(error);
      return done && done(error);
    }
  });

  // others are active, complete, failed, delayed
  // you may want to fetch each id to get the Job object out of it...
  // eslint-disable-next-line
  _queue.failed((err, ids) => {
    ids.forEach((id) => {
      // eslint-disable-next-line
      kue.Job.get(id, (err, job) => {
        job.inactive();
      });
    });
  });
};
module.exports = sendMail;
