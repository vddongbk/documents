const kue = require('kue');
const debug = require('../../config/debug');
const utils = require('../utils');

const taxiTracking = (data) => {
  // eslint-disable-next-line
  const job = _queue.create('tracking', data)
    .priority('high')
    .ttl(7200) // 2 minutes
    .attempts(5)
    .backoff(true)
    .removeOnComplete(true)
    .on('start', () => {
      debug('Driver tracking', job.id, 'Driver', job.data.driverId, 'is start');
    })
    .on('progress', () => {
      debug('Job tracking', job.id, 'Driver', job.data.driverId, 'is progress');
    })
    .on('remove', () => {
      debug('Job tracking', job.id, 'Driver', job.data.driverId, 'is remove');
    })
    .on('complete', () => {
      debug('Job tracking', job.id, 'Driver', job.data.driverId, 'is done');
    })
    .on('failed', () => {
      debug('Job tracking', job.id, 'Driver', job.data.driverId, 'has failed');
      kue.Job.get(job.id, (err, findjob) => {
        if (err) return;
        findjob.remove();
      });
    })
    .save((err) => {
      if (err) {
        debug(`create job err: ${job.id}`);
      }
    });

  // eslint-disable-next-line
  _queue.process('tracking', async (jobProcess, done) => {
    try {
      const { driverId, latitude, longitude } = jobProcess.data;
      const result = await utils.taxiTracking(driverId, latitude, longitude);
      debug(result);
      return done && done();
    } catch (error) {
      debug(error);
      return done && done(error);
    }
  });
};
module.exports = taxiTracking;
