const kue = require('kue');

const config = require('./config');

const queue = kue.createQueue({
  prefix: 'q',
  redis: {
    port: config.redis.port,
    host: config.redis.host,
    db: config.redis.db
  }
});

queue.watchStuckJobs();

if (config.env === 'development') kue.app.listen(3000);

queue.on('error', (err) => {
  // eslint-disable-next-line
  _debug('There was an error in the main queue!');
  // eslint-disable-next-line
  _debug(err);
  // eslint-disable-next-line
  _debug(err.stack);
});

global._queue = queue;

module.exports = queue;
