### QUEUE JOBS

## Implement

```js
const { sendMailJob } = require('./jobs');
```

## Usage

```js
sendMailJob({
      title: 'Send mail reset password',
      to: email,
      code: verifyCode,
      type: 'forgetPassword',
    });
```

## Files

- jobs/ (Jobs service)
- queue.js (Queue config)
- README.md (note)

## Modules

- [kue](https://www.npmjs.com/package/kue)

## References
