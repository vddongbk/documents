const admin = require('firebase-admin');
const config = require('./config.js');

admin.initializeApp({
  credential: admin.credential.cert(config.firebase.serviceAccount),
  databaseURL: config.firebase.databaseUrl,
});

module.exports = {
  sendToDevice(data, registrationToken) {
    let payload = {
      data,
      notification: {
        title: '',
        icon: 'ic_launcher',
        body: '',
        sound: 'default',
      },
    };

    // Send a message to the device corresponding to the provided
    // registration token.
    admin.messaging().sendToDevice(registrationToken, payload)
      .then((response) => {
        // See the MessagingDevicesResponse reference documentation for
        // the contents of response.
        console.log('Successfully sent message:', response);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
  },

  subscribeToTopic(registrationToken, topic) {
    // Subscribe the device corresponding to the registration token to the
    // topic.
    admin.messaging().subscribeToTopic(registrationToken, topic)
      .then((response) => {
        // See the MessagingTopicManagementResponse reference documentation
        // for the contents of response.
        console.log('Successfully subscribed to topic:', response);
      })
      .catch((error) => {
        console.log('Error subscribing to topic:', error);
      });
  },

  unsubscribeFromTopic(registrationToken, topic) {
    // Unsubscribe the device corresponding to the registration token from
    // the topic.
    admin.messaging().unsubscribeFromTopic(registrationToken, topic)
      .then((response) => {
        // See the MessagingTopicManagementResponse reference documentation
        // for the contents of response.
        console.log('Successfully unsubscribed from topic:', response);
      })
      .catch((error) => {
        console.log('Error unsubscribing from topic:', error);
      });
  },

  sendToTopic(data, topic) {
    const payload = {
      data,
      notification: {
        title: '',
        icon: 'ic_launcher',
        body: '',
        sound: 'default',
      },
    };
    // Send a message to devices subscribed to the provided topic.
    admin.messaging().sendToTopic(topic, payload)
      .then((response) => {
        // See the MessagingTopicResponse reference documentation for the
        // contents of response.
        console.log('Successfully sent message:', response);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
  }
};
