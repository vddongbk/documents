### FIREBASE SERVICE

## Implement

```js
const FirebaseService = require('./FirebaseService.js');
```

## Usage

```js
FirebaseService.sendToDevice(data, registrationToken);
```

## Files

- FirebaseService.js (Firebase Service)
- config.js (Config Firebase)
- README.md (note)

## Modules

- [firebase-admin](https://www.npmjs.com/package/firebase-admin)

## References

