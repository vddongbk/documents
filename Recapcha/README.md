### Recapcha Service

## Implement

```js
const RecapchaService = require('./RecapchaService.js');
```

## Usage

```js
const response = await RecapchaService.verify(code);
// return true or false
// note error duplicate code
```

## Files

- RecapchaService.js (Recapcha Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [https](https://www.npmjs.com/package/https)

## References
