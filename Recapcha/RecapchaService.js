"use strict";

const https = require("https");

function verifyRecaptcha(verifyUrl) {
    return new Promise((resolve) => {
        https.get(verifyUrl, function(res) {
            let data = "";
            res.on('data', function (chunk) {
                data += chunk.toString();
            });
            res.on('end', function() {
                try {
                    let parsedData = JSON.parse(data);
                    resolve(parsedData.success);
                } catch (e) {
                    resolve(false)
                }
            });
        });
    });
}

module.exports = {
    /**
     * Verify recapcha
     * @param code g-recaptcha-response
     * @returns {Promise<*>}
     */
    verify: async (code) => {
        try {
            let key = process.env.RECAPCHA_SECRET_KEY,
                verifyUrl = `${process.env.RECAPCHA_URL}?secret=${key}&response=${code}`;

            return await verifyRecaptcha(verifyUrl);
        }
        catch (error) {
            console.log('Error when verify re captcha');
            return false;
        }
    }

};
