### HTML ELEMENT DOM

## Implement

```js
const cheerio = require("cheerio");
```

## Usage

*Example code:*

```js
let urlWeather = `https://www.wunderground.com/history/airport/VVNB/${year}/${month}/${date}/DailyHistory.html?req_city=Noibai+International&req_state=HN&req_statename=Viet+Nam&reqdb.zip=00000&reqdb.magic=966&reqdb.wmo=WVVNB`;
let weatherHtmlContent = await HttpService.get(urlWeather);
let $ = cheerio.load(weatherHtmlContent);

let mainRows = `#obsTable tbody tr`;
let length = $(`${mainRows}`).length;
let numberRow = 0;
let timeFinish = `${hourFinish}:00 ${timeAT}`;
if (minuteFinish >= 30)
    timeFinish = `${hourFinish}:30 ${timeAT}`;

for (let i = 1; i <= length; i++) {
    let mainRow = `#obsTable tbody tr:nth-child(${i})`;
    let time = $(`${mainRow} td:nth-child(1)`).html();
    if (time === timeFinish) {
        numberRow = i;
        break;
    }
}

// get number column
let mainTHead = `#obsTable thead tr th`;
let lengthTHead = $(`${mainTHead}`).length;
let pressureColumn = 0, // hPa
    temperatureColumn = 0, // oC
    humidityColumn = 0; // %
for (let i = 1; i <= lengthTHead; i++) {
    let head = `#obsTable thead tr th:nth-child(${i})`;
    let title = $(`${head}`).html();
    if (title === 'Temp.') {
        temperatureColumn = i;
    } else if (title === 'Humidity') {
        humidityColumn = i;
    } else if (title === 'Pressure') {
        pressureColumn = i;
    }
}

let offsetRows = 0, // head row of table
    mainRowGet = Number(offsetRows) + Number(numberRow); //the row we will get data

// in html element dom format
mainRowGet = `#obsTable tbody tr:nth-child(${mainRowGet})`;

let humidity = $(`${mainRowGet} td:nth-child(${humidityColumn})`).html();
let temperature = $(`${mainRowGet} td:nth-child(${temperatureColumn}) .wx-value`).html();
let pressure = $(`${mainRowGet} td:nth-child(${pressureColumn}) .wx-value`).html();

let numberB = String(pressure) + String(temperature) + String(humidity);
numberB = numberB.replace(/\./g, '');
numberB = numberB.replace(/\%/g, '');
```

## Files

- README.md (note)

## Modules

- [request](https://www.npmjs.com/package/request)

## References

