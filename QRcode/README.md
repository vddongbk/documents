### QRcode Service

## Implement

```js
const QRService = require('./QRService.js');
```

## Usage

```js
const response = await QRService.createQrCode(content);
// return url link
```

## Files

- QRService.js (QRcode Service)
- README.md (note)

## Modules

- [qrcode](https://www.npmjs.com/package/qrcode)

## References
