"use strict";
const path = require("path");

const UploadService = {
    uploadImage: (req, fileName) => {
        return new Promise((resolve, reject) => {
            let approvalStatus = req.user.kyc.approvalStatus;
            if (approvalStatus === undefined && !req._fileparser.upstreams.length)
                return reject(sails.helpers.generateError(sails.errors.requiredIdImages));

            // check file extension
            let filename = req.file(fileName)._files[0].stream.filename,
                extension = filename.split('.').pop().toLowerCase(),
                allowExtensions = sails.config.upload.allowExtensions;
            if (allowExtensions.indexOf(extension) === -1) {
                let error = new Error();
                error.code = sails.config.upload.fileTypeErrorCode;
                error.message = 'Format is not allowed';
                return reject(error);
            }

            let options = {
                maxBytes: sails.config.maxFileSizeUpload,
                dirname: path.join(__dirname, '../..', 'assets/uploads')
            };

            if (sails.config.environment !== 'development') {
                let optionsS3 = {
                    adapter: require('skipper-s3'),
                    key: sails.config.s3.key,
                    secret: sails.config.s3.secret,
                    bucket: sails.config.s3.bucket,
                    region: sails.config.s3.region,
                    headers: {
                        'x-amz-acl': 'public-read'
                    }
                };

                req.file(fileName).upload(optionsS3, (err, filesUploaded) => {
                    if (err) return reject(err);
                    resolve(filesUploaded);
                });
            } else {
                req.file(fileName).upload(options, (err, uploaded) => {
                    if (err) return reject(err);
                    resolve(uploaded);
                })
            }
        })
    }
};

module.exports = UploadService;
