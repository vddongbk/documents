### Upload Service for Sails

## Implement

```js
const UploadService = require('UploadService');
const DeleteImageService = require('DeleteImageService');
```

## Usage

```js
// delete
DeleteImageService.deleteOldImage(urlImage, options);

// upload
let options = {
    req: req,
    inputName: 'banner',
    config: {}
};
UploadService.upload(optionsUpload);

```

## Files

- UploadService.js (Upload Service)
- DeleteImageService.js (Delete Image Service)
- simple/UplodaService.js (simple upload Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [aws-sdk](https://www.npmjs.com/package/aws-sdk)
- [sharp](https://www.npmjs.com/package/sharp)
- [fs](https://www.npmjs.com/package/fs)
- [path](https://www.npmjs.com/package/path)

## References
