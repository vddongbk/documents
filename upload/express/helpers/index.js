module.exports = {
    filterExtension: (typeFile) => {
        const type = typeFile.split('/');
        let extension = type[1];
        switch (extension) {
            case 'vnd.openxmlformats-officedocument.presentationml.presentation':
                extension = 'pptx';
                break;
            default:
                break;
        }
        return extension;
    }
};
