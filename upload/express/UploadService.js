const formidable = require('formidable');
const path = require('path');
const sharp = require('sharp');
const assert = require('assert');
const uuid = require('uuid');
const fs = require('fs');

const helpers = require('./helpers');

// const aws = require('aws-sdk');
// const s3 = new aws.S3({signatureVersion: 'v4'})
// const Transform = require('stream').Transform

// aws.config.update({
//   accessKeyId: '',
//   secretAccessKey: '',
//   region: 'ap-southeast-1'
// });

module.exports = {
  upload: (req, nameField = 'file', options) => {
    assert(req, 'UploadService - "req" is required');
    assert(nameField, 'UploadService - "nameField" is required');
    return new Promise((resolve, reject) => {
      let form = new formidable.IncomingForm();
      const { types, maxSize, isArray, typeUpload } = options;
      const dataStoreOrigin = [];
      const dataStoreThumb = [];

      /**
       * Options
       */
      form = Object.assign(form, {
        multiples: true,
        keepExtensions: true,
        // uploadDir: path.join(__dirname, '../../', './.tmp'), // Set standard upload dir
        encoding: 'utf-8',
        type: 'multipart', // or urlencoded
        maxFieldsSize: 20 * 1024 * 1024, // default = 20 * 1024 * 1024 = 20mb
        maxFields: 1000, // Max files & fields - default = 1000
        hash: false, // sha1, md5 or false
        // @note - Disable field & file event listeners and let you handle upload yourself
        onPart(part) {
          // eslint-disable-next-line
          part.addListener('data', (packet) => {
            // console.log('Packet received', packet.toString()); // Raw packet data
            // packet_a + packet_b + packet_c + ... = file data
          });
          // Handle part / file only if .mov is not included in filename
          if (part.filename && part.filename.indexOf('.mov') === -1) {
            form.handlePart(part);
            // Or if filename is not set
          } else if (!part.filename) {
            form.handlePart(part);
          }
        }
      });

      /**
       * Events
       */
      // eslint-disable-next-line
      form.on('fileBegin', async (name, file) => {
        // file.name - basename with extension
        // file.size - currently uploaded bytes
        // file.path - beeing written to
        // file.type - mime
        // console.log(file.size);
        if (!types.includes(file.type)) {
          form._error(new Error('type not allows'));
          form.pause();
          return reject(new Error('type not allows'));
        }

        if (form.bytesExpected > maxSize) {
          form._error(new Error('file too larger'));
          form.pause();
          return reject(new Error('file too larger'));
        }

        // if (name > maxSize) {
        //   form._error(new Error('file too larger'));
        //   form.pause();
        //   return reject(new Error('file too larger'));
        // }

        const filename = uuid.v1();
        const time = new Date().getTime();
        const extension = helpers.filterExtension(file.type);
        // eslint-disable-next-line
        file.path = path.join(__dirname, '../../', `./public/uploads/origin/${filename}-${time}.${extension}`);
        dataStoreOrigin.push({
          origin: `${process.env.DOMAIN}/uploads/origin/${filename}-${time}.${extension}`,
        });
        // console.log(file.size);
        // console.log(file);
        // console.log(name);
        // file.lastModifiedDate - date object or null
        // file.hash - hex digest if set
        // Changing file upload path can also be done here:
        // file.path = path.join(__dirname, '../../', './.tmp/second' + file.name)
        // console.log(file);
        // const img = sharp(file);
        // console.log(img);
        // let bufferThumb = await file.resize(200).embed().toBuffer();
        // console.log(bufferThumb);
        // file.on('error', e => this._error(e))
        //
        // file.open = function () {
        //   this._writeStream =  new Transform({
        //     transform (chunk, encoding, callback) {callback(null, chunk)}
        //   })
        //
        //   this._writeStream.on('error', e => this.emit('error', e))
        //
        //   s3.upload({
        //     Bucket: 'tokubuy',
        //     Key: 'test.jpg',
        //     Body: this._writeStream
        //   }, onUpload)
        // }
        //
        // file.end = function (cb) {
        //   this._writeStream.on('finish', () => {
        //     this.emit('end', cb());
        //   })
        //   this._writeStream.end()
        // }

        // continue execution in here
        // function onUpload (err, res) {
        //   err ? console.log('error:\n', err) : console.log('response:\n', res)
        // }
      });
      form.on('progress', (bytesReceived, bytesExpected) => {
        console.log('Progress:', bytesReceived, bytesExpected);
      });
      form.on('error', (err) => {
        console.log(err);
        reject(err);
      });
      form.on('aborted', () => {
        reject(new Error('Aborted'));
      });
      form.on('end', () => {
        console.log('End');
      });
      form.on('field', (name, value) => {
        console.log('Field', name, value);
      });
      form.on('file', (name, file) => {
        console.log('Field', name, file);
      });

      /**
       * Function
       *
       * Passes request from express to formidable for handling.
       * Second arg is a callback executed on complete & returns all data
       *
       */
      // eslint-disable-next-line
      form.parse(req, async(err, fields, files) => {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          let dataStore = [];
          if (typeUpload === 'image') {
            if (Array.isArray(files[nameField])) {
              for (let i = 0; i < files[nameField].length; i += 1) {
                const file = files[nameField][i];
                const filename = uuid.v1();
                const time = new Date().getTime();
                const extension = helpers.filterExtension(file.type);
                const pathThumb = path.join(__dirname, '../../', `./public/uploads/thumb/${filename}-${time}.${extension}`);
                dataStoreThumb.push({
                  thumb: `${process.env.DOMAIN}/uploads/thumb/${filename}-${time}.${extension}`
                });
                // eslint-disable-next-line
                sharp(file.path).resize(640, 360).max().toBuffer((error, buffer) => {
                  if (error) {
                    fs.unlinkSync(file.path);
                    reject(new Error('Error in-progress upload'));
                  } else {
                    fs.writeFileSync(pathThumb, buffer);
                  }
                });
              }
            } else {
              const file = files[nameField];
              const filename = uuid.v1();
              const time = new Date().getTime();
              const extension = helpers.filterExtension(file.type);
              const pathThumb = path.join(__dirname, '../../', `./public/uploads/thumb/${filename}-${time}.${extension}`);
              dataStoreThumb.push({
                thumb: `${process.env.DOMAIN}/uploads/thumb/${filename}-${time}.${extension}`
              });
              // eslint-disable-next-line
              sharp(file.path).resize(640, 360).max().toBuffer((error, buffer) => {
                if (error) {
                  fs.unlinkSync(file.path);
                  reject(new Error('Error in-progress upload'));
                } else {
                  fs.writeFileSync(pathThumb, buffer);
                }
              });
            }
          }
          dataStoreOrigin.forEach((item, index) => {
            const data = Object.assign(item, dataStoreThumb[index]);
            dataStore.push(data);
          });
          if (!isArray) dataStore = dataStore && dataStore[0];
          resolve(dataStore);
        }
      });
    });
  }
};
