### Upload Service

## Implement

```js
const UploadService = require('./UploadService.js'); // SNS AWS
```

## Usage

*IOS*

```js
const result = await UploadService.upload(req, 'file', {
      types: [
        'image/jpg',
        'image/png',
        'audio/mp3',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      ],
      maxSize: 20 * 1024 * 1024,
      isArray: true,
      typeUpload: 'image'
    });
```

## Files

- UploadService.js (Upload Service)
- helpers/index.js (Helper Functions)
- .env (Environment variables)
- README.md (note)

## Modules

- [formidable](https://www.npmjs.com/package/formidable)
- [path](https://www.npmjs.com/package/path)
- [sharp](https://www.npmjs.com/package/sharp)
- [assert](https://www.npmjs.com/package/assert)
- [uuid](https://www.npmjs.com/package/uuid)
- [fs](https://www.npmjs.com/package/fs)

## References
