const fs = require('fs');
const uuid = require('uuid');
const sharp = require('sharp');

const imageFilter = function (fileName) {
  // accept image only
  if (!fileName.match(/\.(jpg|jpeg|png|gif)$/)) {
    return false;
  }

  return true;
};

const _fileHandler = function (file, options) {
  if (!file) throw new Error('no_file');

  if (options.fileFilter && !options.fileFilter(file.hapi.filename)) {
    throw new Error('type_not_allowed');
  }

  const originalname = file.hapi.filename;
  const filename = uuid.v1();
  const time = new Date().getTime();
  const extension = originalname.split('.').pop().toLowerCase();
  const path = `${options.origin}${filename}-${time}.${extension}`;
  const fileStream = fs.createWriteStream(path);
  const hostname = process.env.DOMAIN_PUBLIC || null;
  const origin = `${hostname}/images/origin/${filename}-${time}.${extension}`;
  const filenameThumb = uuid.v1();
  const pathThumb = `${options.thumb}${filenameThumb}-${time}.${extension}`;
  let thumb = `${hostname}/images/thumbnail/${filenameThumb}-${time}.${extension}`;

  return new Promise((resolve, reject) => {
    file.on('error', (err) => {
      reject(err);
    });

    file.pipe(fileStream);

    file.on('end', (err) => {
      if (err) {
        fs.unlinkSync(path);
        reject(new Error('err_in_progress_upload'));
      } else if (options.mini) {
        thumb = origin;
      } else {
        sharp(path).resize(200, 200).max().toBuffer((error, buffer) => {
          if (error) {
            fs.unlinkSync(path);
            reject(new Error('err_in_progress_upload'));
          } else {
            fs.writeFileSync(pathThumb, buffer);
          }
        });
      }

      const fileDetails = {
        // originalname,
        // filename,
        // mimetype: file.hapi.headers['content-type'],
        // destination: `${options.dest}`,
        // path,
        // size: fs.statSync(path).size,
        origin,
        thumb,
      };
      resolve(fileDetails);
    });
  });
};

const _filesHandler = function (files, options) {
  if (!files || !Array.isArray(files)) throw new Error('no_files');

  const promises = files.map(x => _fileHandler(x, options));
  return Promise.all(promises);
};

const uploader = function (file, options) {
  if (!file) throw new Error('no_file');

  return Array.isArray(file) ? _filesHandler(file, options) : _fileHandler(file, options);
};

module.exports = { imageFilter, uploader };
