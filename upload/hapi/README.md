### Upload Service (Local) for Hapi 

## Implement

```js
const { imageFilter, uploader } = require('UploadService');
```

## Usage

*create path (run in-progressing start server)*
```js
const fs = require('fs');

if (!fs.existsSync(process.env.UPLOAD_PATH)) fs.mkdirSync(process.env.UPLOAD_PATH);
if (!fs.existsSync(process.env.UPLOAD_PATH_ORIGIN)) fs.mkdirSync(process.env.UPLOAD_PATH_ORIGIN);
if (!fs.existsSync(process.env.UPLOAD_PATH_THUMB)) fs.mkdirSync(process.env.UPLOAD_PATH_THUMB);
```

*Add payload option handler*
```js
payload: {
  output: 'stream',
  allow: 'multipart/form-data', // important
  parse: true,
  maxBytes: 1048576,
}
```

*usage*

```js
const { imageFilter, uploader } = require('UploadService');

const fileOptions = {
  origin: `${process.env.UPLOAD_PATH_ORIGIN}/`,
  thumb: `${process.env.UPLOAD_PATH_THUMB}/`,
  fileFilter: imageFilter,
  mini: false, // if true that image thumb same image origin
};

const fileDetails = await uploader(image, fileOptions);
```
*note (to view image uploaded that we need public them, config into folder: public)*

    /public

## Files

- SNSService.js (SNS AWS Service)
- TwilioService.js (Twilio Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [aws-sdk](https://www.npmjs.com/package/aws-sdk)
- [twilio](https://www.npmjs.com/package/twilio)

## References

- [twilio.com](https://www.twilio.com/)
