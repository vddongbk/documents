const Routes = require('./routes');

const register = (server) => {
  server.route(Routes);
  server.log('info', 'Plugin registered: public-plugins');
};

exports.plugin = {
  name: 'public-plugins',
  version: '1.0.0',
  register,
};
