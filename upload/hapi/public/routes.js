const Handler = require('./handler');

const Routes = [
  // Images
  {
    method: 'GET',
    path: '/origin/{name_image*}',
    config: Handler.publicImages,
    handler: {
      directory: {
        path: 'uploads/origin',
      },
    },
  },
  {
    method: 'GET',
    path: '/thumbnail/{name_image*}',
    config: Handler.publicImages,
    handler: {
      directory: {
        path: 'uploads/thumb',
      },
    },
  },
];

module.exports = Routes;
