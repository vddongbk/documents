/**
 * Email Service
 * @description Server-side logic for Email handle
 */

/**
 * Switch Email Adapter by environment
 */
let sendMail = null;
if (process.env.NODE_ENV === 'development') {
    sendMail = require('./GmailService');
} else {
    sendMail = require('./SESService');
}

if (!sendMail) sails.log.error('Not Found Email Adapter for EmailService');

module.exports = {

    /**
     * Example
     * @param {string} email
     * @return {Promise}
     */
    example: (email) => {
        let options = {
            to: email,
            from: 'Example <info@example.net>',
            subject: `Example title`,
            html:  `<h3>Hello</h3>`
        };
        return sendMail(options);
    }
};
