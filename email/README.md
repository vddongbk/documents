### EMAIL SERVICE - SEND MAIL

## Implement

```js
const fse = require("fs-extra");
const path = require("path");
const events = require("events");

/** Global EventEmitter */
global.EventEmitter = new events.EventEmitter();


    /**
     * Implement Event/Listener like Laravel
     */
    let eventHandlersDir = path.join(__dirname, '..', 'events');
    let allEventHandleFiles = await fse.readdir(eventHandlersDir);
    for (let eventHandleFile of allEventHandleFiles) {
        let eventHandle = require(path.join(eventHandlersDir, eventHandleFile));
        for (let listener of eventHandle) {
            EventEmitter.on(listener.eventName, (data) => {
                console.log(`${eventHandleFile.replace('.js', '')} - Executing event: ${listener.eventName}`);
                listener.handler(data);
            })
        }
    }
```

## Usage

```js
EventEmitter.emit('name_event', params);
```

## Files

- events/EmailEventHandle.js (Email event listener)
- EmailService.js (Email service use Gmail or SES)
- GmailService.js (Gmail Service)
- SESService.js (SES AWS Service)
- .env (environment variables)
- README.md (note)

## Modules

- [nodemailer](https://www.npmjs.com/package/nodemailer)
- [nodemailer-ses-transport](https://www.npmjs.com/package/nodemailer-ses-transport)
- [events](https://www.npmjs.com/package/events)
- [path](https://www.npmjs.com/package/path)
- [fs-extra](https://www.npmjs.com/package/fs-extra)

## References
