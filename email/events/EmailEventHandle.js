"use strict";

/**
 * Event Listener: Handle Email Event
 * [{
 *    eventName: 'model.action',
 *    handler: function(data){}
 * }]
 */
const EmailService = require('../EmailService');

module.exports = [
    {
        eventName: 'example',
        handler: async(email) => {
            try {
                // do something here...

                await EmailService.example(email);
            } catch (error) {
                console.log('EmailEvent Error:', error);
            }
        }
    },
];
