### Redis Service

## Implement

```js
const RedisService = require('./RedisService.js');
```

## Usage

```js
let value = await RedisService.get(key_redis);
```

## Files

- RedisService.js (Redis Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [redis](https://www.npmjs.com/package/redis)

## References

- [Redis.io](https://redis.io/)
