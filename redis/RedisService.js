"use strict";

const redis = require('redis');
const config = require('../../config/config');
const assert = require('assert');
const { promisify } = require('util');

const client = redis.createClient(config.redis);

const redisGet = promisify(client.get).bind(client);
const redisSet = promisify(client.set).bind(client);
const redisDel = promisify(client.del).bind(client);
const redisIncr = promisify(client.incr).bind(client);
const redisTtl = promisify(client.ttl).bind(client);
const redisExists = promisify(client.exists).bind(client);
const redisSadd = promisify(client.sadd).bind(client);
const redisSmembers = promisify(client.smembers).bind(client);
const redisSpop = promisify(client.spop).bind(client);
const redisScard = promisify(client.scard).bind(client);
const redisSort = promisify(client.sort).bind(client);
const redisClean = promisify(client.flushall).bind(client);

module.exports = {

    /**
     *
     * @param key
     * @returns {Promise<any>}
     */
    get: (key) => {
        assert(key, 'Redis service - method get - "key" is require');
        return redisGet(key);
    },

    /**
     *
     * @param key
     * @param data
     * @param expiresIn
     * @returns {Promise<any>}
     */
    stringsSet: (key, data, expiresIn) => {
        assert(key, 'Redis service - method stringSet - "key" is require');
        assert(data, 'Redis service - method stringSet - "data" is require');
        return redisSet(key, data, 'EX', expiresIn);
    },

    /**
     *
     * @param key
     * @param data
     * @returns {Promise<any>}
     */
    stringsSetNoExpires: (key, data) => {
        assert(key, 'Redis service - method stringsSetNoExpires - "key" is require');
        assert(data, 'Redis service - method stringsSetNoExpires - "data" is require');
        return redisSet(key, data);
    },

    /**
     *
     * @param key
     * @returns {Promise<any>}
     */
    delete: (key) => {
        assert(key, 'Redis service - method delete - "key" is require');
        return redisDel(key);
    },

    /**
     *
     * @param key
     * @returns {Promise<any>}
     */
    increment: (key) => {
        assert(key, 'Redis service - method increment - "key" is require');
        return redisIncr(key);
    },

    /**
     *
     * @param key
     * @returns {Promise<any>}
     */
    getKeyRemainingTime: (key) => {
        assert(key, 'Redis service - method getKeyRemainingTime - "key" is require');
        return redisTtl(key);
    },

    /**
     *
     * @param key
     * @returns {Promise<any>}
     */
    checkExist: (key) => {
        assert(key, 'Redis service - method checkExist - "key" is require');
        return redisExists(key);
    },

    /**
     *
     * @param key
     * @param data
     * @returns {Promise<any>}
     */
    sadd: (key, data) => {
        assert(key, 'Redis service - method sadd - "key" is require');
        assert(data, 'Redis service - method sadd - "data" is require');
        return redisSadd(key, data);
    },

    /**
     *
     * @param key
     * @returns {Promise<any>}
     */
    smembers: (key) => {
        assert(key, 'Redis service - method smembers - "key" is require');
        return redisSmembers(key);
    },

    /**
     * Remove and return one or multiple random members from a set
     * @param key: redis key
     * @param amount: number of members
     * @return {Promise.<array>} array members removed
     */
    spop: (key, amount) => {
        assert(key, 'Redis service - method spop - "key" is require');
        assert(amount, 'Redis service - method spop - "amount" is require');
        return redisSpop(key, amount);
    },

    scard: (key) => {
        assert(key, 'Redis service - method scard - "key" is require');
        return redisScard(key);
    },

    /**
     *
     * @param key
     * @param sort
     * @param limit
     * @returns {Promise<any>}
     */
    sort: (key, sort = 'DESC', limit = 5) => {
        assert(key, 'Redis service - method sort - "key" is require');
        return redisSort(key, sort, 'by', 'total_*', 'limit', 0, limit);
    },

    /**
     *
     * @returns {*}
     */
    clean: () => redisClean()
};
