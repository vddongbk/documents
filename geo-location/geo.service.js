const redis = require('redis');
const assert = require('assert');
const geolib = require('geolib');

const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    db: process.env.REDIS_DB,
    prefix: process.env.REDIS_PREFIX
});

const geo = require('georedis').initialize(client, {
  zset: 'locations',
  nativeGeo: true
});

module.exports = {
  /**
   *
   * @param name
   * @returns {Promise<any>}
   */
  addSet: (name) => {
    assert(name, 'GeoService - "name" is required');
    const promise = new Promise((resolve, reject) => {
      try {
        const addNewSet = geo.addSet(name);
        resolve(addNewSet);
      } catch (err) {
        reject(err);
      }
    });
    return promise;
  },
  /**
   *
   * @param name
   * @returns {Promise<any>}
   */
  deleteSet: (name) => {
    assert(name, 'GeoService - "name" is required');
    const promise = new Promise((resolve, reject) => {
      try {
        const addNewSet = geo.deleteSet(name);
        resolve(addNewSet);
      } catch (err) {
        reject(err);
      }
    });
    return promise;
  },
  /**
   *
   * @param name
   * @param latitude
   * @param longitude
   * @param model
   * @returns {Promise<any>}
   */
  addOneLocation: (name, latitude, longitude, model = geo) => {
    assert(name, 'GeoService - "name" is required');
    assert(latitude, 'GeoService - "latitude" is required');
    assert(longitude, 'GeoService - "longitude" is required');
    const promise = new Promise((resolve, reject) => {
      model.addLocation(name, { latitude, longitude }, (err, reply) => {
        if (err) reject(err);
        resolve(reply);
      });
    });
    return promise;
  },
  /**
   *
   * @param name
   * @param latitude
   * @param longitude
   * @param model
   * @returns {Promise<any>}
   */
  updateOneLocation: (name, latitude, longitude, model = geo) => {
    assert(name, 'GeoService - "name" is required');
    assert(latitude, 'GeoService - "latitude" is required');
    assert(longitude, 'GeoService - "longitude" is required');
    const promise = new Promise((resolve, reject) => {
      model.updateLocation(name, { latitude, longitude }, (err, reply) => {
        if (err) reject(err);
        resolve(reply);
      });
    });
    return promise;
  },
  /**
   *
   * @param locations
   * @param model
   * @returns {Promise<any>}
   */
  addMultiLocation: (locations, model = geo) => {
    assert(locations, 'GeoService - "locations" is required');
    const promise = new Promise((resolve, reject) => {
      model.addLocations(locations, (err, reply) => {
        if (err) reject(err);
        resolve(reply);
      });
    });
    return promise;
  },
  /**
   *
   * @param locations
   * @param model
   * @returns {Promise<any>}
   */
  updateMultiLocation: (locations, model = geo) => {
    assert(locations, 'GeoService - "locations" is required');
    const promise = new Promise((resolve, reject) => {
      model.updateLocations(locations, (err, reply) => {
        if (err) reject(err);
        resolve(reply);
      });
    });
    return promise;
  },
  /**
   *
   * @param name
   * @param model
   * @returns {Promise<any>}
   */
  getOneLocation: (name, model = geo) => {
    assert(name, 'GeoService - "name" is required');
    const promise = new Promise((resolve, reject) => {
      model.location(name, (err, location) => {
        if (err) reject(err);
        resolve(location);
      });
    });
    return promise;
  },
  /**
   *
   * @param arrayName
   * @param model
   * @returns {Promise<any>}
   */
  getMultiLocation: (arrayName, model = geo) => {
    assert(arrayName, 'GeoService - "arrayName" is required');
    const promise = new Promise((resolve, reject) => {
      model.locations(arrayName, (err, locations) => {
        if (err) reject(err);
        resolve(locations);
      });
    });
    return promise;
  },
  /**
   *
   * @param latitude
   * @param longitude
   * @param distance
   * @param model
   * @param options
   * @returns {Promise<any>}
   */
  // eslint-disable-next-line
  nearByLocation: (latitude, longitude, distance, model = geo, options) => {
    assert(latitude, 'GeoService - "latitude" is required');
    assert(longitude, 'GeoService - "longitude" is required');
    assert(distance, 'GeoService - "distance" is required');
    if (!options) {
      // eslint-disable-next-line
      options = {
        withCoordinates: true, // Will provide coordinates with locations, default false
        withHashes: true, // Will provide a 52bit Geohash Integer, default false
        withDistances: true, // Will provide distance from query, default false
        order: 'ASC', // or 'DESC' or true (same as 'ASC'), default false
        units: 'km', // or 'km', 'mi', 'ft', default 'm'
        count: 100, // Number of results to return, default undefined
        accurate: true // Useful if in emulated mode and accuracy is important, default false
      };
    }
    const promise = new Promise((resolve, reject) => {
      model.nearby({ latitude, longitude }, distance, options, (err, locations) => {
        if (err) reject(err);
        resolve(locations);
      });
    });
    return promise;
  },
  /**
   *
   * @param name
   * @param model
   * @returns {Promise<any>}
   */
  removeOneLocation: (name, model = geo) => {
    assert(name, 'GeoService - "name" is required');
    const promise = new Promise((resolve, reject) => {
      model.removeLocation(name, (err, reply) => {
        if (err) reject(err);
        resolve(reply);
      });
    });
    return promise;
  },
  /**
   *
   * @param arrayName
   * @param model
   * @returns {Promise<any>}
   */
  removeMultiLocation: (arrayName, model = geo) => {
    assert(arrayName, 'GeoService - "arrayName" is required');
    const promise = new Promise((resolve, reject) => {
      model.removeLocations(arrayName, (err, reply) => {
        if (err) reject(err);
        resolve(reply);
      });
    });
    return promise;
  },
  /**
   *
   * @param fromLatitude
   * @param fromLongitude
   * @param toLatitude
   * @param toLongitude
   * @reference https://github.com/manuelbieh/Geolib
   * @returns {*}
   */
  getDistance: (fromLatitude, fromLongitude, toLatitude, toLongitude) => {
    assert(fromLatitude, 'GeoService - "fromLatitude" is required');
    assert(fromLongitude, 'GeoService - "fromLongitude" is required');
    assert(toLatitude, 'GeoService - "toLatitude" is required');
    assert(toLongitude, 'GeoService - "toLongitude" is required');
    const distance = geolib.getDistance(
      { latitude: fromLatitude, longitude: fromLongitude },
      { latitude: toLatitude, longitude: toLongitude }
    );
    return distance;
  },
  /**
   *
   * @param arrCoordinates
   * @example
   * [
   *   { latitude: 52.516272, longitude: 13.377722 },
   *   { latitude: 51.515, longitude: 7.453619 },
   *   { latitude: 51.503333, longitude: -0.119722 }
   * ]
   *
   * OR
   *
   * {
   *   "Brandenburg Gate, Berlin": {latitude: 52.516272, longitude: 13.377722},
   *   "Dortmund U-Tower": {latitude: 51.515, longitude: 7.453619},
   *   "London Eye": {latitude: 51.503333, longitude: -0.119722},
   *   "Kremlin, Moscow": {latitude: 55.751667, longitude: 37.617778},
   *   "Eiffel Tower, Paris": {latitude: 48.8583, longitude: 2.2945},
   *   "Riksdag building, Stockholm": {latitude: 59.3275, longitude: 18.0675},
   *   "Royal Palace, Oslo": {latitude: 59.916911, longitude: 10.727567}
   * }
   * @reference https://github.com/manuelbieh/Geolib
   * @returns {*}
   */
  getCenter: (arrCoordinates) => {
    assert(arrCoordinates, 'GeoService - "arrCoordinates" is required');
    const center = geolib.getCenter(arrCoordinates);
    return center;
  }
};
