### Geo location Service

## Implement

```js
const GeoService = require('./GeoService.js');
```

## Usage

```js
const taxi = await GeoService.addSet('taxi');
await GeoService.deleteSet(name);
await GeoService.removeOneLocation(name, taxi);
...
```

## Files

- geo.service.js (GEO location Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)

## References
