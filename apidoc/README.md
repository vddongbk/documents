### API Documents

## Implement

`npm i apidoc -g`

```js
- package.json
 "scripts": {
    "doc": "apidoc -i docs/ -o public/apidoc/"
 }
```

## Usage

`npm run doc` or `yarn doc`

## Files

- docs/ (docs comment)
- apidoc.json (Config doc)
- README.md (note)

## Modules

- [apidoc](https://www.npmjs.com/package/apidoc)

## References
- https://www.npmjs.com/package/apidoc
