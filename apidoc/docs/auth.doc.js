module.exports = {
  /**
   * @api {post} /auth/login Login
   * @apiVersion 1.0.0
   * @apiPermission user, driver
   * @apiName Login
   * @apiGroup AUTH
   *
   * @apiParam {String} [email] Email of the User.<br><code>required malformed</code>
   * @apiParam {String} [password] Password of the User.<br><code>required min:6 max:255</code>
   * @apiParam {String} [deviceToken] Device that user using.<br><code>required</code>
   * @apiParamExample {json} Request-Example:
   * {
   *   "email": "d2d@d2d.net",
   *   "password": "123123",
   *   "deviceToken": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp"
   * }
   *
   * @apiSuccess {Number} [statusCode=200] status response.
   * @apiSuccess {String} [data=token] data response.
   * @apiSuccess {String} [message="Success"] message success.
   * @apiSuccess {Number} [code=0] value code.
   * @apiSuccessExample Success Response:
   * {
   *   "statusCode": 200,
   *   "data": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp",
   *   "message": "Success",
   *   "error": 0
   * }
   *
   * @apiError {Number} [statusCode=400,422,500] status response.
   * @apiError {Boolean} error message error common http-request.
   * @apiError {String} message message error response.
   * @apiError {Number=1,2,3,10,11} [code=1,2,3,10,11] value code.
   * @apiErrorExample Error email required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error email is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error email must be a valid email:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" must be a valid email",
   *   "code": 1
   * }
   * @apiErrorExample Error email not exist:
   * {
   *   "statusCode": 404,
   *   "error": "Not Found",
   *   "message": "email not exists",
   *   "code": 10
   * }
   * @apiErrorExample Error password required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"password\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error password is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"password\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error password length must be at least 6 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"password\" length must be at least 6 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error password length must be less than or equal to 255 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"password\" length must be less than or equal to 255 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error email or password incorrect:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "email or password incorrect",
   *   "code": 11
   * }
   * @apiErrorExample Error deviceToken required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"deviceToken\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error deviceToken is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"deviceToken\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error login fail:
   * {
   *   "statusCode": 400,
   *   "message": "Bad Request",
   *   "error": "Bad Request",
   *   "code": 2
   * }
   * @apiErrorExample Error server:
   * {
   *   "statusCode": 500,
   *   "message": "An internal server error occurred",
   *   "error": "Internal Server Error",
   *   "code": 3
   * }
   */

  /**
   * @api {post} /auth/signup Register
   * @apiVersion 1.0.0
   * @apiPermission user, driver
   * @apiName Register
   * @apiGroup AUTH
   *
   * @apiParam {String} [email] Email of the User.<br><code>required malformed</code>
   * @apiParam {String} [password] Password of the User.<br><code>required min:6 max:255</code>
   * @apiParam {String} [username] Username of the User.<br><code>required min:3 max:20</code>
   * @apiParam {String} [birthday] Birthday of the User.<br><code>required malformed</code>
   * @apiParam {String} [gender] Gender of the User.<br><code>required integer min:1 max:3</code>
   * @apiParam {String} [phone] Phone of the User.<br><code>required malformed</code>
   * @apiParam {String} [isDriver] Role use app.<br><code>required boolean default: false</code>
   * @apiParam {String} [identifier] Role use app.<br><code>required if isDriver: true</code>
   * @apiParam {String} [company] Role use app.<br><code>required if isDriver: true</code>
   * @apiParam {String} [type] Role use app.<br><code>required if isDriver: true</code>
   * @apiParam {String} [isCalling] Role use app.<br><code>boolean default: true</code>
   * @apiParam {String} [isBooking] Role use app.<br><code>boolean default: true</code>
   * @apiParam {String} [isAirport] Role use app.<br><code>boolean default: true</code>
   * @apiParamExample {json} Request-Example:
   * {
   *   "email": "d2d@d2d.net",
   *   "password": "123123",
   *   "username": "d2d",
   *   "birthday": "",
   *   "gender": 1,
   *   "phone": "01637272123",
   *   "isDriver": true,
   *   "identifier": "43H123344",
   *   "company": "5b3439b1fb0b4f39e6172892",
   *   "type": "5b3439b1fb0b4f39e6172894",
   *   "isCalling": true,
   *   "isBooking": true,
   *   "isAirport": true
   * }
   *
   * @apiSuccess {Number} [statusCode=200] status response.
   * @apiSuccess {String} [data=token] data response.
   * @apiSuccess {String} [message="Success"] message success.
   * @apiSuccess {Number} [code=0] value code.
   * @apiSuccessExample Success Response:
   * {
   *   "statusCode": 200,
   *   "data": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp",
   *   "message": "Success",
   *   "error": 0
   * }
   *
   * @apiError {Number} [statusCode=400,422,500] status response.
   * @apiError {Boolean} error message error common http-request.
   * @apiError {String} message message error response.
   * @apiError {Number=1,2,3,14,15,18,19} [code=1,2,3,14,15,18,19] value code.
   * @apiErrorExample Error email required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error email is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error email must be a valid email:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" must be a valid email",
   *   "code": 1
   * }
   * @apiErrorExample Error email has exists:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "email has exists",
   *   "code": 12
   * }
   * @apiErrorExample Error password required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"password\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error password length must be at least 6 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"password\" length must be at least 6 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error password length must be less than or equal to 255 characters long:
   * {
   *   "statusCode": 422,
   *   "message": "\"password\" length must be less than or equal to 255 characters long",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error username is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"username\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error username length must be at least 3 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"username\" length must be at least 3 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error username length must be less than or equal to 20 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"username\" length must be less than or equal to 20 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error username has exists:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "username has exists",
   *   "code": 14
   * }
   * @apiErrorExample Error birthday is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"birthday\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error birthday invalid:
   * {
   *   "statusCode": 422,
   *   "message": "\"birthday\" must be a string with one of the following formats [YYYY-MM-DD, YYYY/MM/DD]",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error birthday invalid:
   * {
   *   "statusCode": 422,
   *   "message": "\"birthday\" must be a number of milliseconds or valid date string",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error birthday invalid:
   * {
   *   "statusCode": 422,
   *   "message": "\"birthday\" must be less than or equal to \"Fri Jun 29 2018 15:18:19 GMT+0700 (+07)\"",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error gender is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"gender\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error gender must be a number:
   * {
   *   "statusCode": 422,
   *   "message": "\"gender\" must be a number",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error gender must be less than or equal to 3:
   * {
   *   "statusCode": 422,
   *   "message": "\"gender\" must be less than or equal to 3",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error gender must be larger than or equal to 1:
   * {
   *   "statusCode": 422,
   *   "message": "\"gender\" must be larger than or equal to 1",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error gender must be an integer:
   * {
   *   "statusCode": 422,
   *   "message": "\"gender\" must be an integer",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error phone is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"phone\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error phone did not seem to be a phone number:
   * {
   *   "statusCode": 422,
   *   "message": "\"phone\" did not seem to be a phone number",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error isDriver must be a boolean:
   * {
   *   "statusCode": 422,
   *   "message": "\"isDriver\" must be a boolean",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error identifier required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"identifier\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error identifier is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"identifier\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error company required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"company\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error company is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"company\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error company id malformed:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"company\" must only contain hexadecimal characters",
   *   "code": 1
   * }
   * @apiErrorExample Error company id malformed:
   * {
   *   "statusCode": 400,
   *   "error": "Unprocessable Entity",
   *   "message": "id malformed",
   *   "code": 20
   * }
   * @apiErrorExample Error company not exists:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "company not exists",
   *   "code": 15
   * }
   * @apiErrorExample Error type required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"type\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error type is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"type\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error type id malformed:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"type\" must only contain hexadecimal characters",
   *   "code": 1
   * }
   * @apiErrorExample Error type id malformed:
   * {
   *   "statusCode": 400,
   *   "error": "Unprocessable Entity",
   *   "message": "id malformed",
   *   "code": 20
   * }
   * @apiErrorExample Error type not exists:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "type not exists",
   *   "code": 18
   * }
   * @apiErrorExample Error type not has in company:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "type has not in company",
   *   "code": 19
   * }
   * @apiErrorExample Error isCalling must be a boolean:
   * {
   *   "statusCode": 422,
   *   "message": "\"isCalling\" must be a boolean",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error isBooking must be a boolean:
   * {
   *   "statusCode": 422,
   *   "message": "\"isBooking\" must be a boolean",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error isAirport must be a boolean:
   * {
   *   "statusCode": 422,
   *   "message": "\"isAirport\" must be a boolean",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error register fail:
   * {
   *   "statusCode": 400,
   *   "message": "Bad Request",
   *   "error": "Bad Request",
   *   "code": 2
   * }
   * @apiErrorExample Error server:
   * {
   *   "statusCode": 500,
   *   "message": "An internal server error occurred",
   *   "error": "Internal Server Error",
   *   "code": 3
   * }
   */

  /**
   * @api {post} /auth/forget_password/email Email Forget Password
   * @apiVersion 1.0.0
   * @apiPermission user, driver
   * @apiName Email Forget Password
   * @apiGroup AUTH
   *
   * @apiParam {String} [email] email of the User.<br><code>required malformed</code>
   * @apiParamExample {json} Request-Example:
   * {
   *   "email": "d2d@d2d.net"
   * }
   *
   * @apiSuccess {Number} [statusCode=200] status response.
   * @apiSuccess {String} [data=verifyToken] data response.
   * @apiSuccess {String} [message="Success"] message success.
   * @apiSuccess {Number} [code=0] value code.
   * @apiSuccessExample Success Response:
   * {
   *   "statusCode": 200,
   *   "data": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp",
   *   "message": "Success",
   *   "error": 0
   * }
   *
   * @apiError {Number} [statusCode=400,422,500] status response.
   * @apiError {Boolean} error message error common http-request.
   * @apiError {String} message message error response.
   * @apiError {Number=1,2,3,10,35} [code=1,2,3,10,35] value code.
   * @apiErrorExample Error email required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error email is not allowed to be empty:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" is not allowed to be empty",
   *   "code": 1
   * }
   * @apiErrorExample Error email must be a valid email:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"email\" must be a valid email",
   *   "code": 1
   * }
   * @apiErrorExample Error email not exists:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "email not exists",
   *   "code": 10
   * }
   * @apiErrorExample Error limit reset password:
   * {
   *   "statusCode": 400,
   *   "error": "Bad Request",
   *   "message": "limit reset password",
   *   "code": 35
   * }
   * @apiErrorExample Error get fail:
   * {
   *   "statusCode": 400,
   *   "message": "Bad Request",
   *   "error": "Bad Request",
   *   "code": 2
   * }
   * @apiErrorExample Error server:
   * {
   *   "statusCode": 500,
   *   "message": "An internal server error occurred",
   *   "error": "Internal Server Error",
   *   "code": 3
   * }
   */

  /**
   * @api {post} /auth/forget_password/verify Verify Forget Password
   * @apiVersion 1.0.0
   * @apiPermission user, driver
   * @apiName Verify Forget Password
   * @apiGroup AUTH
   *
   * @apiParam {String} [verifyCode] verifyCode.<br><code>required min: 6</code>
   * @apiParam {String} [verifyToken] verifyToken.<br><code>required</code>
   * @apiParamExample {json} Request-Example:
   * {
   *   "verifyCode": "124531",
   *   "verifyToken": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp"
   * }
   *
   * @apiSuccess {Number} [statusCode=200] status response.
   * @apiSuccess {String} [data=null] data response.
   * @apiSuccess {String} [message="Success"] message success.
   * @apiSuccess {Number} [code=0] value code.
   * @apiSuccessExample Success Response:
   * {
   *   "statusCode": 200,
   *   "data": null,
   *   "message": "Success",
   *   "error": 0
   * }
   *
   * @apiError {Number} [statusCode=400,422,500] status response.
   * @apiError {Boolean} error message error common http-request.
   * @apiError {String} message message error response.
   * @apiError {Number=1,2,3,21} [code=1,2,3,21] value code.
   * @apiErrorExample Error verifyCode is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"verifyCode\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error verifyCode length must be at least 6 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"verifyCode\" length must be at least 6 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error verifyToken is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"verifyToken\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error verify code incorrect or time out:
   * {
   *   "statusCode": 422,
   *   "message": "code incorrect or time out",
   *   "error": "Unprocessable Entity",
   *   "code": 21
   * }
   * @apiErrorExample Error get fail:
   * {
   *   "statusCode": 400,
   *   "message": "Bad Request",
   *   "error": "Bad Request",
   *   "code": 2
   * }
   * @apiErrorExample Error server:
   * {
   *   "statusCode": 500,
   *   "message": "An internal server error occurred",
   *   "error": "Internal Server Error",
   *   "code": 3
   * }
   */

  /**
   * @api {put} /auth/forget_password Reset Password
   * @apiVersion 1.0.0
   * @apiPermission user, driver
   * @apiName Reset Password
   * @apiGroup AUTH
   *
   * @apiParam {String} [newPassword] Password of the User.<br><code>required min: 6 max: 255</code>
   * @apiParam {String} [verifyCode] verifyCode.<br><code>required min: 6 max: 6</code>
   * @apiParam {String} [verifyToken] verifyToken.<br><code>required</code>
   * @apiParamExample {json} Request-Example:
   * {
   *   "newPassword": "123456",
   *   "verifyCode": "124531",
   *   "verifyToken": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp"
   * }
   *
   * @apiSuccess {Number} [statusCode=200] status response.
   * @apiSuccess {String} [data=token] data response.
   * @apiSuccess {String} [message="Success"] message success.
   * @apiSuccess {Number} [code=0] value code.
   * @apiSuccessExample Success Response:
   * {
   *   "statusCode": 200,
   *   "data": null,
   *   "message": "Success",
   *   "error": 0
   * }
   *
   * @apiError {Number} [statusCode=400,422,500] status response.
   * @apiError {Boolean} error message error common http-request.
   * @apiError {String} message message error response.
   * @apiError {Number=1,2,3,21} [code=1,2,3,21] value code.
   * @apiErrorExample Error newPassword required:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"newPassword\" is required",
   *   "code": 1
   * }
   * @apiErrorExample Error newPassword length must be at least 6 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"newPassword\" length must be at least 6 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error newPassword length must be less than or equal to 255 characters long:
   * {
   *   "statusCode": 422,
   *   "message": "\"password\" length must be less than or equal to 255 characters long",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error verifyCode is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"verifyCode\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error verifyCode length must be at least 6 characters long:
   * {
   *   "statusCode": 422,
   *   "error": "Unprocessable Entity",
   *   "message": "\"verifyCode\" length must be at least 6 characters long",
   *   "code": 1
   * }
   * @apiErrorExample Error verifyCode length must be less than or equal to 6 characters long:
   * {
   *   "statusCode": 422,
   *   "message": "\"verifyCode\" length must be less than or equal to 6 characters long",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error verifyToken is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"verifyToken\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error verify code incorrect or time out:
   * {
   *   "statusCode": 422,
   *   "message": "code incorrect or time out",
   *   "error": "Unprocessable Entity",
   *   "code": 21
   * }
   * @apiErrorExample Error get fail:
   * {
   *   "statusCode": 400,
   *   "message": "Bad Request",
   *   "error": "Bad Request",
   *   "code": 2
   * }
   * @apiErrorExample Error server:
   * {
   *   "statusCode": 500,
   *   "message": "An internal server error occurred",
   *   "error": "Internal Server Error",
   *   "code": 3
   * }
   */

  /**
   * @api {post} /auth/logout Logout
   * @apiVersion 1.0.0
   * @apiPermission user, driver
   * @apiName Logout
   * @apiGroup AUTH
   *
   * @apiHeader {String} Authorization Token
   * @apiHeaderExample Authorization Example:
   * Authorization : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YWIzMTQzMzcyODg5NjAzOTFhMTMzZTYiLCJpYXQiOjE1MjE2ODc3MjMsImV4cCI6MTUyMjI5MjUyM30.k6_TjBdghiKVKB7EXCROjugrRuSaSn8hQMM7RXdXxxQ"
   *
   * @apiParam {String} [deviceToken] current deviceToken of device .<br><code>required</code>
   * @apiParamExample {json} Request-Example:
   * {
   *   "deviceToken": "7apsIGEwpUFgj6yL5nYuXRWj2rnP9UrzK91w05mRxXGvx5QqQkj0gWZUHN44L9jwkHerkEDp",
   * }
   *
   * @apiSuccess {Number} [statusCode=200] status response.
   * @apiSuccess {String} [data=null] data response.
   * @apiSuccess {String} [message="Success"] message success.
   * @apiSuccess {Number} [code=0] value code.
   * @apiSuccessExample Success Response:
   * {
   *   "statusCode": 200,
   *   "data": null,
   *   "message": "Success",
   *   "error": 0
   * }
   *
   * @apiError {Number} [statusCode=400,422,500] status response.
   * @apiError {Boolean} error message error common http-request.
   * @apiError {String} message message error response.
   * @apiError {Number=1,2,3,5,15,20} [code=1,2,3,5,15,20] value code.
   * @apiErrorExample Error device token is required:
   * {
   *   "statusCode": 422,
   *   "message": "\"deviceToken\" is required",
   *   "error": "Unprocessable Entity",
   *   "code": 1
   * }
   * @apiErrorExample Error Unauthorized:
   * {
   *   "statusCode": 401,
   *   "error": "Unauthorized",
   *   "message": "Unauthorized",
   *   "code": 5
   * }
   * @apiErrorExample Error get fail:
   * {
   *   "statusCode": 400,
   *   "message": "Bad Request",
   *   "error": "Bad Request",
   *   "code": 2
   * }
   * @apiErrorExample Error server:
   * {
   *   "statusCode": 500,
   *   "message": "An internal server error occurred",
   *   "error": "Internal Server Error",
   *   "code": 3
   * }
   */
};
