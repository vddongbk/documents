### Xlxs Service

## Implement

```js
const XlxsService = require('./XlxsService.js');
```

## Usage

```js
XlxsService.write(data, filePath);
// [
//    ['head cell', 'head cell', 'head cell'],
//    ['data', 'data', 'data']
// ]
```

## Files

- XlxsService.js (Xlxs Service)
- README.md (note)

## Modules

- [Xlxs](https://www.npmjs.com/package/xlsx)

## References
