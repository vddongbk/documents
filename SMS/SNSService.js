"use strict";

const aws = require('aws-sdk');
aws.config.update({
    accessKeyId: process.env.SNS_ACCESS_KEY_ID,
    secretAccessKey: process.env.SNS_SECRET_ACCESS_KEY
});
aws.config.setPromisesDependency(global.Promise);
const SNS = new aws.SNS({
    apiVersion: process.env.SNS_API_VERSION,
    region: process.env.SNS_REGION
});

const SmsService = {
    send: async (phone, code) => {
        try {
            let smsParams = {
                Message: `Your activation code is ${code}`,
                PhoneNumber: phone,
                MessageAttributes: {
                    'DefaultSMSType': {
                        DataType: 'String',
                        StringValue: 'Transactional'
                    },
                },
            };

            await SNS.publish(smsParams).promise();
        } catch (error) {
            throw error;
        }

    }
};

module.exports = SmsService;
