### SMS Service

## Implement

```js
const SNSService = require('./SNSService.js'); // SNS AWS
```

*or*

```js
const TwilioService = require('./TwilioService.js'); // Twilio
```

## Usage

```js
SNSService.send(phone, code);
```

*or*

```js
TwilioService.send(phone, code);
```

## Files

- SNSService.js (SNS AWS Service)
- TwilioService.js (Twilio Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [aws-sdk](https://www.npmjs.com/package/aws-sdk)
- [twilio](https://www.npmjs.com/package/twilio)

## References

- [twilio.com](https://www.twilio.com/)
