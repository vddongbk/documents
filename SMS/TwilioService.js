"use strict";

const twilio = require('twilio');
const client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

module.exports = {
    client: client,

    send: (phone, code) => {
        return client.messages.create({
            to: phone,
            from: process.env.TWILIO_PHONE_NUMBER,
            body: "Your verify code is " + code,
        }).then(message => sails.log.info(`Sent sms id: ${message.sid}`))
          .catch(err  => sails.log.error(err));
    }
};
