### DB FIREBASE SERVICE

## Implement

```js
const DBFirebaseService = require('./DBFirebaseService.js');
```

## Usage

```js
DBFirebaseService.addDocument(data);
```

## Files

- DBFirebaseService.js (Database Firebase Service)
- config.js (Config Database Firebase)
- README.md (note)

## Modules

- [firebase-admin](https://www.npmjs.com/package/firebase-admin)

## References


