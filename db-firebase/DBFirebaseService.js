const admin = require('firebase-admin');
const config = require('./config.js');

admin.initializeApp({
    credential: admin.credential.cert(config.firebase.serviceAccount),
    databaseURL: config.firebase.databaseUrl,
});

const db = admin.firestore();

module.exports = {

  // ADD DATA
  addDocument(data) {
    db.collection('notifications').add(data);
  },

  // EDIT
  updateDocument(id, data) {
    const docRef = db.collection('notifications').doc(id);
    docRef.set(data);
  },

  // UPDATE FIELD
  updateDocumentField(id, data) {
    const updateProductRef = db.collection('notifications').doc(id);
    // Set the field into document that data change
    updateProductRef.update(data);
  },

  // GET ALL DATA
  getAllDocuments(page = 1, limit = bap.env.limit, fieldOrder = 'pushedAt', sort = 'DESC') {
    const offset = (page - 1) * limit;
    return db.collection('notifications').orderBy(fieldOrder, sort).offset(offset).limit(limit)
      .get()
      .then((snapshot) => {
        const docs = [];
        snapshot.forEach((doc) => {
          docs.push(doc.data());
        });
        return docs;
      })
      .catch((err) => {
        console.log('Error getting documents', err);
      });
  },

  // GET A DOCUMENT
  getDocumentById(id) {
    const productRef = db.collection('notifications').doc(id);
    return productRef.get()
      .then((doc) => {
        if (!doc.exists) {
          console.log('No such document!');
        }
        return doc.data();
      })
      .catch((err) => {
        console.log('Error getting document', err);
      });
  },

  // GET MULTIPLE DOCUMENTS FROM A COLLECTION
  getMultipleDocuments(query) {
    const notifiesRef = db.collection('notifications');
    return notifiesRef.where(query).get()
      .then((snapshot) => {
        const docs = [];
        snapshot.forEach((doc) => {
          docs.push(doc.data());
        });
        return docs;
      })
      .catch((err) => {
        console.log('Error getting documents', err);
      });
  },
};
