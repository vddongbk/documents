### 1PAY

## Implement

```js
const OnePayService = require('./1PayService.js');
```

## Usage

*Example code*

```js
/**
 * TransactionController
 *
 * @description :: Server-side logic for managing auctions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const uuidv1 = require('uuid/v1');
const crypto = require('crypto');

module.exports = {

    /**
     * Get Card Transaction
     */
    getCardTransaction: asyncWrap(async (req, res) => {
        let userId = req.user._id;
        let serial = '';
        let pin = '';
        let type = '';
        res.render('transaction/card', {userId, serial, pin, type, isSuccess: null});
    }),

    /**
     * Post Card Transaction
     */
    postCardTransaction: asyncWrap(async (req, res) => {
        let type = req.body.lstTelco;
        let pin = req.body.txtCode;
        let serial = req.body.txtSeri;
        let userid  = req.body.userId;

        let dataRes = await OnePayService.cardTransaction(type, pin, serial);
        let isSuccess = false;
        if (dataRes.status === '00') {
            // fee transaction buy Card from 1Pay
            let amount = dataRes.amount;
            let restAmount = await CardRepository.fee1PayCard(type, amount);
            let coin = (restAmount*sails.config.value1Chance)/sails.config.value1k;
            coin = parseInt(coin);
            if (await CoinRepository.updateCoinOnePay(userid, coin)) {
                isSuccess = true;
                // save log
                await LogUserBuyCoin.create({
                    user: userid,
                    coin: coin,
                    money: parseInt(dataRes.amount),
                    type: LogUserBuyCoin.type.card
                });
            }
            serial = '';
            pin = '';
            type = '';
        }

        if (isSuccess)
        sails.sockets.blast('addcoin', JSON.stringify({isSuccess: isSuccess}));

        req.flash('dataRes', dataRes);
        res.render('transaction/card', {userId:userid, serial, pin, type, isSuccess});
    }),

    /**
     * Get Local Bank Transaction
     */
    getLocalBankTransaction: asyncWrap(async (req, res) => {
        let userId = req.user._id;
        let order_id = uuidv1();
        let amount = '';
        let orderInfor = '';
        res.render('transaction/local_bank', {order_id, userId, amount, orderInfor, isSuccess: null});
    }),

    /**
     * Post Local Bank Transaction
     */
    postLocalBankTransaction: asyncWrap(async (req, res) => {
        let amount = req.body.amount;
        let orderId = req.body.order_id;
        let orderInfor = req.body.order_info;
        let userId = req.body.userId;

        let dataRes = await OnePayService.postLocalBankTransaction(amount, orderId, orderInfor, userId);

        // redirect pay_url 1pay
        res.redirect(dataRes.pay_url);
    }),

    bankResult: asyncWrap(async (req, res) => {
        // receive response from 1pay buy return_url
        let trans_ref = req.query.trans_ref,
            response_code = req.query.response_code,
            userId = req.params.userid;
        
        let isSuccess = false;
        //Ex url: http://api.1pay.vn/bank-charging/bank_result/userid?access_key=l6apnlfseia0ooa12gwp&amount=10000&card_name=Ng%C3%A2n+h%C3%A0ng+TMCP+Ngo%E1%BA%A1i+th%C6%B0%C6%A1ng+Vi%E1%BB%87t+Nam&card_type=VCB&order_id=001&order_info=test+dich+vu&order_type=ND&request_time=2014-12-30T17%3A50%3A11Z&response_code=00&response_message=Giao+dich+thanh+cong&response_time=2014-12-30T17%3A52%3A12Z&signature=eb7aef260a18c835582964e840d63f68b9f84d9704bac7b16c8ff7f1ac9bd0d8&trans_ref=44df289349c74a7d9690ad27ed217094&trans_status=finish

        let commitRes = await OnePayService.bankResult(trans_ref, response_code);

        // Ex: {"amount":10000,"trans_status":"close","response_time": "2014-12-31T00:52:12Z","response_message":"Giao dịch thành công","response_code":"00","order_info":"test dich vu","order_id":"001","trans_ref":"44df289349c74a7d9690ad27ed217094", "request_time":"2014-12-31T00:50:11Z","order_type":"ND"}

        if(commitRes.response_code === "00")
        {
            // charge success
            // handle fee from 1Pay
            let amountTransaction = parseInt(commitRes.amount);
            let restAmount = sails.helpers.feeLocalBank1Pay(amountTransaction);
            let coin = (restAmount*sails.config.value1Chance)/sails.config.value1k;
            coin = parseInt(coin);
            if (await CoinRepository.updateCoinOnePay(userId, coin)) {
                isSuccess = true;
                // save log
                await LogUserBuyCoin.create({
                    user: userId,
                    coin: coin,
                    money: parseFloat(commitRes.amount),
                    type: LogUserBuyCoin.type.bank
                });
            }
        } else {
            // charge errors
            req.flash('dataRes', commitRes.response_message);
        }
        let order_id = uuidv1();
        sails.sockets.blast('addcoin', JSON.stringify({isSuccess: isSuccess}));
        let amount = '';
        let orderInfor = '';
        res.render('transaction/local_bank', {userId, isSuccess, order_id, amount, orderInfor});
    })
};
```

## Files

- 1PayService.js (1Pay Service)
- helper.js (Helper Function)
- HttpService.js (Http Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [uuid](https://www.npmjs.com/package/uuid)
- [crypto](https://www.npmjs.com/package/crypto)
- [request](https://www.npmjs.com/package/request)

## References

