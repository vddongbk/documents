/**
 * This file export many useful function
 */

const uuidv1 = require('uuid/v1');
const crypto = require('crypto');

module.exports = {

    /**
     * 1Pay card
     */
    cardData1Pay: (lstTelco, txtCode, txtSeri) => {
        let transRef = uuidv1()
            ,access_key = process.env.ONE_PAY_ACCESS_KEY
            ,secret = process.env.ONE_PAY_SECRET
            ,type = lstTelco
            ,pin = txtCode
            ,serial = txtSeri
            ,data = "access_key=" + access_key + "&pin=" +pin + "&serial=" +serial +  "&transRef=" + transRef + "&type=" +type
            ,signature = crypto.createHmac('sha256', secret).update(data).digest('hex');

        return {
            access_key:access_key,
            type:type,
            pin:pin,
            serial:serial,
            transRef:transRef,
            signature:signature
        };
    },

    /**
     * 1Pay local bank
     */
    localBankData1Pay: (amountCharge, orderId, orderInfor) => {
        let access_key = process.env.ONE_PAY_ACCESS_KEY,
            secret = process.env.ONE_PAY_SECRET,
            return_url = `${process.env.ONE_PAY_CALLBACK_URL}/${userId}`,
            command = 'request_transaction',
            amount = amountCharge,   // >10000
            order_id = orderId,
            order_info = orderInfor,
            data = "access_key=" + access_key + "&amount=" + amount + "&command=" + command + "&order_id=" + order_id + "&order_info=" + order_info + "&return_url=" + return_url,
            signature = crypto.createHmac('sha256', secret).update(data).digest('hex');

        return {
            access_key: access_key,
            amount: amount,
            command: command,
            order_id: order_id,
            order_info: order_info,
            return_url: return_url,
            signature: signature
        };
    },

    /**
     * Handle subtract fee transaction by local bank form 1Pay
     * @param amount
     * @return restAmount
     */
    feeLocalBank1Pay: (amount) => {
        const DTA = amount;
        const TLB = 98.5;   // ratio share from 1Pay
        const fee = 1100;   // fee transaction
        let DT = DTA/1.1;

        let DTB = (DT * TLB)/100; //excluding VAT
        DTB = DTB - fee;

        // method register with 1Pay: Persional
        let DTBPer10 = (DTB * 10)/100; // Excluding VAT
        DTB = DTB - DTBPer10; // Including VAT
        return DTB;
    },
};
