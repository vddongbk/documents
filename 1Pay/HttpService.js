"use strict";

const request = require("request");

const HttpService = {

    /**
     *
     * @param uri
     * @return {Promise}
     */
    post1Pay: (uri, formData) => {
        return new Promise((resolve, reject) => {
            let options = {
                method: 'post',
                uri: uri,
                json: true,
                form: formData
            };
            request.post(options, (error, response, body) => {
                if (error) reject(error);
                else resolve(body)
            })
        })
    }

};

module.exports = HttpService;
