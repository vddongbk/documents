const Helper = require('./helper');
const HttpService = require('./HttpService');
module.exports = {
    /**
     * Post Card Transaction
     */
    cardTransaction: async(type, pin, serial) => {
        try {
            let formData = Helper.cardData1Pay(type, pin, serial);

            return await HttpService.post1Pay(process.env.ONE_PAY_API_CARD, formData);
        } catch (err) {
            throw err;
        }
    },

    /**
     * Post Local Bank Transaction
     */
    postLocalBankTransaction: async (amount, orderId, orderInfor) => {
        try {
            let formData = sails.helpers.localBankData1Pay(amount, orderId, orderInfor);

            return await HttpService.post1Pay(process.env.ONE_PAY_API_LOCAL_BANK, formData);

            // redirect pay_url 1pay
            // res.redirect(dataRes.pay_url);
        } catch (err) {
            throw err;
        }
    },

    bankResult: async(trans_ref, response_code) => {
        try {
            // receive response from 1pay buy return_url
            let access_key = process.env.ONE_PAY_ACCESS_KEY,
                secret = process.env.ONE_PAY_SECRET,
                return_url = process.env.ONE_PAY_CALLBACK_URL;

            if (response_code === "00") {
                let command = "close_transaction",
                    data = "access_key=" + access_key + "&command=" + command + "&trans_ref=" + trans_ref,
                    signature = crypto.createHmac('sha256', secret).update(data).digest('hex');

                let formData = {
                    access_key: access_key,
                    command: command,
                    trans_ref: trans_ref,
                    signature: signature
                };
                //Ex url: http://api.1pay.vn/bank-charging/bank_result/userid?access_key=l6apnlfseia0ooa12gwp&amount=10000&card_name=Ng%C3%A2n+h%C3%A0ng+TMCP+Ngo%E1%BA%A1i+th%C6%B0%C6%A1ng+Vi%E1%BB%87t+Nam&card_type=VCB&order_id=001&order_info=test+dich+vu&order_type=ND&request_time=2014-12-30T17%3A50%3A11Z&response_code=00&response_message=Giao+dich+thanh+cong&response_time=2014-12-30T17%3A52%3A12Z&signature=eb7aef260a18c835582964e840d63f68b9f84d9704bac7b16c8ff7f1ac9bd0d8&trans_ref=44df289349c74a7d9690ad27ed217094&trans_status=finish

                return await HttpService.post1Pay(process.env.ONE_PAY_API_COMMIT_LOCAL_BANK, formData);

                // Ex: {"amount":10000,"trans_status":"close","response_time": "2014-12-31T00:52:12Z","response_message":"Giao dịch thành công","response_code":"00","order_info":"test dich vu","order_id":"001","trans_ref":"44df289349c74a7d9690ad27ed217094", "request_time":"2014-12-31T00:50:11Z","order_type":"ND"}
            } else {
                return {
                    response_code: 1000,
                    response_message: 'error transaction'
                }
            }
        } catch (err) {

        }
    }
}
