### SMS Service

## Implement

```js
const SNSService = require('./SNSService.js'); // SNS AWS
```

## Usage

*IOS*

```js
SNSService.notificationIOS: (deviceToken, title, dataSend);
```

## Files

- SNSService.js (SNS AWS Service)
- .env (Environment variables)
- README.md (note)

## Modules

- [aws-sdk](https://www.npmjs.com/package/aws-sdk)
- [assert](https://www.npmjs.com/package/assert)

## References
