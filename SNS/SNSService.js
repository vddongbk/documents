const AWS = require('aws-sdk');
const assert = require('assert');

AWS.config.update({
  accessKeyId: process.env.SNS_ACCESS_KEY_ID,
  secretAccessKey: process.env.SNS_SECRET_ACCESS_KEY,
});

AWS.config.setPromisesDependency(global.Promise);

const sns = new AWS.SNS({
  apiVersion: process.env.SNS_VERSION,
  region: process.env.SNS_REGION
});

module.exports = {
  notificationIOS: (deviceToken, title, dataSend) => {
    assert(deviceToken, 'SNSService - "deviceToken" is required');
    assert(title, 'SNSService - "title" is required');
    assert(dataSend, 'SNSService - "dataSend" is required');
    return new Promise((resolve, reject) => {
      sns.createPlatformEndpoint({
        PlatformApplicationArn: process.env.SNS_PLATFORM_APPLICATION_ARN,
        Token: deviceToken
      }, (err, data) => {
        if (err) reject(err.stack);

        const endpointArn = data.EndpointArn;
        let payload = {
          default: 'D2D Platform',
          APNS_VOIP: {
            aps: {
              alert: title,
              sound: 'default',
              badge: 1
            },
            d2d: dataSend
          },
        };

        // first have to stringify the inner APNS object...
        payload.APNS_VOIP = JSON.stringify(payload.APNS_VOIP);
        // then have to stringify the entire message payload
        payload = JSON.stringify(payload);
        const params = {
          Attributes: {
            Enabled: 'true',
          },
          EndpointArn: endpointArn,
        };
        // eslint-disable-next-line
        sns.setEndpointAttributes(params, (error, data) => {
          if (error) reject(error);
          sns.publish({
            Message: payload,
            MessageStructure: 'json',
            TargetArn: endpointArn
          }, (errPublish, dataPublish) => {
            if (errPublish) reject(errPublish.stack);
            resolve(dataPublish);
          });
        });
      });
    });
  },
};
