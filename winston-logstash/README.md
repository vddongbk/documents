### WINSTON LOGSTASH

## Implement

```js
const logger = require('LogService');
```

## Usage

```js
logger.log('info', 'Server start!');
logger.info('Hello log with metas',{color: 'blue', geo: {country:'France',city: "Paris"} });
```

## Files

- LogService.js (Log Service)
- config.js (Config Service)
- filelog-infor.log (file write log if log by file type)
- .env (Environment variables)
- README.md (note)

## Modules

- [winston](https://www.npmjs.com/package/winston)
- [winston-logstash](https://www.npmjs.com/package/winston-logstash)

## References
