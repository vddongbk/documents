'use strict';

const winston = require('winston');
require('winston-logstash');

const logConfig = require('./config.js');

let logger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)(logConfig.file)
    ]
});

if (process.env.NODE_ENV !== 'development') {
    logger = new (winston.Logger)({
        transports: [
            new (winston.transports.Logstash)(logConfig.logStash)
        ]
    });
}

module.exports = logger;
