'use strict';

module.exports = {

    file: {
        name: 'info-file',
        filename: 'filelog-info.log',
        level: 'info'
    },

    logStash: {
        port: process.env.WINSTON_LOGSTASH_PORT,
        ssl_enable: false,
        host: process.env.WINSTON_LOGSTASH_HOST,
        max_connect_retries: -1,
        node_name: process.env.WINSTON_LOGSTASH_NODE,
    },
};
