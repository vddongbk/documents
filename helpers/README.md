### Helpers function

## Implement


## Usage

## Function helper

>*generateError*

```js
    /**
     * Return Error instance
     * @param data - of error will create
     *      message: display when log this error
     *      code: access when by error.code
     * @return {object} error instance
     */
    generateError: (data) => {
        let {message, code} = data;
        let error = new Error(message);
        if (code) error.code = code;
        return error;
    }
```

>*randomNumber*

```js
    /**
     * Return a random number
     * @param length - number of characters - default 6 - max 15
     * @returns {string}
     */
    randomNumber: (length = 6) => {
        if (length > 15) length = 15;
        let result = '';
        for (let i = 0; i < length; i++) {
            let randomNumber = Math.floor(Math.random() * 10);
            result += String(randomNumber);
        }
        return result;
    }
```

>*getIpAddress*

```js
    const ip = require("ip");

    /**
     * Return request Ip Address
     * @return {string}
     */
    getIpAddress: () => {
        return ip.address();
    }
```

>*getDevice*

```js
    const MobileDetect = require("mobile-detect");

    /**
     * Return User Device
     * @param header - request header user-agent
     * @return {*|String|string} e.g. 'desktop', 'ios', 'android' or 'undefined'
     */
    getDevice: (header) => {
        let detect = new MobileDetect(header),
            device = detect.os();
        return device || 'undefined';
    }
```

>*getSkipItemByPage*

```js
    /**
     * Return skip value in query pagination
     *    e.g Post.find({
     *          limit: 10,
     *          skip: getSkipItemByPage(page)
     *        })
     * @param page
     * @return {number}
     */
    getSkipItemByPage: (page = 1, limit = 10) => {
        return ((page - 1) * limit)
    }
```

>*calculatorTotalPages*

```js
    /**
     * Calculator total pages
     * @param totalDocument
     */
    calculatorTotalPages: (totalDocument = 0, limit = 10) => {
        return Math.ceil(totalDocument / limit);
    }
```
>*isMongoId*

```js
    const mongoose = require("mongoose");
    const ObjectId = mongoose.Types.ObjectId;
    
    /**
     * Check if string is mongodb id
     * @param id
     * @return {boolean}
     */
    isMongoId:(id) => {
        return ObjectId.isValid(id);
    }
```

>*toObjectId*

```js
    const mongoose = require("mongoose");
    const ObjectId = mongoose.Types.ObjectId;
    
    /**
     * Convert string to MongoDb ObjectId
     * @param {string} id
     */
    toObjectId: (id) => {
        return mongoose.Types.ObjectId(id);
    }
```

>*optionPaginate*

```js
    /**
     *
     * @param populate
     * [{
     *  path: '',
     *  populate: [{
     *  
     *  }]
     *  select: ''
     * }]
     * @param page
     * @param sort
     * {
     *  createdAt: -1
     * }
     * @param limit
     */
    optionPaginate : (page = 1, populate, sort, limit = 10) => {
        return {
            lean: true,
            sort: sort,
            populate: populate,
            limit: limit,
            page: page
        }
    }
```

>*transferToArrayValue*

```js
    /**
     * Convert Array Object to Array Value
     * @param arrayObject
     * @param property
     */
    transferToArrayValue: (arrayObject, property = '_id') => {
        let outputArray = [];
        arrayObject.forEach(object => {
            if (object[property] !== undefined) outputArray.push(object[property]);
        });
        return outputArray;
    }
```

>*getNameImage*

```js
    /**
     *
     * @param urlImage
     * @return name
     */
    getNameImage: (urlImage) => {
        let pathArr = urlImage.split('/');
        let name = pathArr[pathArr.length - 1];
        return name;
    }
```

>*getNumberItemLoad*

```js
    /**
     *
     * @param page
     * @param arrayLength
     * @param limit
     * @return numberItems
     */
    getNumberItemLoad: (page, arrayLength, limit = 10) => {
        let numberItems = limit*page;
        if (numberItems > arrayLength)
            numberItems = arrayLength;
        return numberItems;
    }
```

>*makeRandoms*

```js
    makeRandoms: (notThis) => {
        let randoms = [0,3];
        
        // faster way to remove an array item when you don't care about array order
        function removeArrayItem(i) {
            let val = randoms.pop();
            if (i < randoms.length) {
                randoms[i] = val;
            }
        }
        
        function makeRandom() {
            let rand = randoms[Math.floor(Math.random() * randoms.length)];
            removeArrayItem(rand);
            return rand;
        }
        
        // remove the notThis item from the array
        if (notThis < randoms.length) {
            removeArrayItem(notThis);
        }
        
        return makeRandom();
    }
```

>*minuteFromNow*

```js
    const moment = require('moment');

    /**
     * Get minute from Date now
     */
    minuteFromNow: (startTime) => {
        try {
            const end = moment();
            const duration = moment.duration(end.diff(startTime));
            return duration.asMinutes();
        } catch (error) {
            throw error;
        }
    }
```

>*now*

```js
    /**
    * Get time Date now
    */
    now: () => {
        try {
            return moment().format();
        } catch (error) {
            throw error;
        }
    }
```

>*isNumber*

```js
    /**
     * 
     * @param number
     * @returns {boolean}
     */
    isNumber(number) {
        let valid = true;
        if (number === null) {
            valid = false;
        } else if (number === undefined) {
            valid = false;
        } else if (!Number(number)) {
            valid = false;
        }
        return valid;
    }
```

>*getPage*

```js
    /**
     *
     * @param page
     * @return {*}
     */
    getPage: (page) => {
        if (!page || page < 0 || isNaN(page)) page = 1;
        return page;
    }
```

## Function config

>*asyncWrap*

*- Implement*

```js
/**
 * Global asyncWrap function
 * Because express isn't promise-aware, you have
 * to use a wrapping function to catch any errors
 * @param fn - async function - e.g. async (req, res, next){ do await action }
 * @param errorCallback - callback to handle error
 */
global.asyncWrap = (fn, errorCallback) => {
    return (req, res, next) => {
        fn(req, res, next).catch(error => {
            if (errorCallback){
                errorCallback(req, res, error);
            } else {
                console.log(error);
                if (process.env.NODE_ENV === 'production') {
                    return res.json({status: 400, message: 'bad request', code: 0});
                }
                res.json({status: 500, message: 'server error', code: 0});
            }
        })
    }
};
```

*- Usage*

```js
const example = asyncWrap(async (req, res) => {
        // Do something here...
        res.json();
    }, (req, res, error) => {
        if (error.code === define.code)
            return res.json({status: 400, message: error.message, code: error.code});
        res.json({status: 500, message: 'server error', code: 0});
    });
```

>*Moment*

```js
    /**
     * Global Moment and set timezone to Japan time
     */
    const moment = require("moment-timezone");
    moment.tz.setDefault('Asia/Ho_Chi_Minh');
    global.moment = moment;
```

>*Auto global file into folder*

```js
    const fse = require("fs-extra");
    const path = require("path");
    
    /**
     * Global Model, then you can use User, Product.. in your controllers, polices..
     */
    let modelDir = path.join(__dirname, '..', 'api/models');
    let allModelFiles = await fse.readdir(modelDir);
    for (let modelFile of allModelFiles) {
        let modelGlobalName = modelFile.replace('.js', '');
        global[modelGlobalName] = require(path.join(modelDir, modelFile));
    }
```

>*Auto glocal event emitter*

```js
    global.EventEmitter = new events.EventEmitter();


    /**
     * Implement Event/Listener like Laravel
     */
    let eventHandlersDir = path.join(__dirname, '..', 'events');
    let allEventHandleFiles = await fse.readdir(eventHandlersDir);
    for (let eventHandleFile of allEventHandleFiles) {
        let eventHandle = require(path.join(eventHandlersDir, eventHandleFile));
        for (let listener of eventHandle) {
            EventEmitter.on(listener.eventName, (data) => {
                console.log(`${eventHandleFile.replace('.js', '')} - Executing event: ${listener.eventName}`);
                listener.handler(data);
            })
        }
    }
```

>*Mongoose*

```js
    /**
     * Connect MongoDb by Mongoose package
     */
    mongoose.Promise = global.Promise;
    let connect = mongoose.connect(url_mongodb, {useMongoClient: true});
    mongoose.connection.on('error', () => {
        throw new Error('Mongodb Connection Error!');
    });
```

## Files

- XlxsService.js (Xlxs Service)
- README.md (note)

## Modules

- [events](https://www.npmjs.com/package/events)
- [path](https://www.npmjs.com/package/path)
- [fs-extra](https://www.npmjs.com/package/fs-extra)
- [moment-timezone](https://www.npmjs.com/package/moment-timezone)
- [moment](https://www.npmjs.com/package/moment)
- [mongoose](https://www.npmjs.com/package/mongoose)
- [ip](https://www.npmjs.com/package/ip)
- [mobile-detect](https://www.npmjs.com/package/mobile-detect)

## References
