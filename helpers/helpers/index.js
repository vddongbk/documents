/**
 * This file export many useful function
 */

const ip = require("ip");
const MobileDetect = require("mobile-detect");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const moment = require('moment');

module.exports = {
    /**
     * Return a random number
     * @param length - number of characters - default 6 - max 15
     * @returns {string}
     */
    randomNumber: (length = 6) => {
        if (length > 15) length = 15;
        let result = '';
        for (let i = 0; i < length; i++) {
            let randomNumber = Math.floor(Math.random() * 10);
            result += String(randomNumber);
        }
        return result;
    },

    /**
     * Return request Ip Address
     * @return {string}
     */
    getIpAddress: () => {
        return ip.address();
    },

    /**
     * Return User Device
     * @param header - request header user-agent
     * @return {*|String|string} e.g. 'desktop', 'ios', 'android' or 'undefined'
     */
    getDevice: (header) => {
        let detect = new MobileDetect(header),
            device = detect.os();
        return device || 'undefined';
    },

    /**
     * Convert Array Object to Array
     * input = [{id: 1, name: 'foo'}, {id: 2, name: 'baz'}]
     *    arrayObjectToArrayKey(input)          => [1, 2]
     *    arrayObjectToArrayKey(input, 'name')  => ['foo', 'baz']
     * @param arrayObject
     * @param property
     */
    arrayObjectToArrayValue: (arrayObject, property = '_id') => {
        let outputArray = [];
        arrayObject.forEach(object => {
            if (object.hasOwnProperty(property)) outputArray.push(object[property]);
        });
        return outputArray;
    },

    /**
     * Return skip value in query pagination
     *    e.g Post.find({
     *          limit: 10,
     *          skip: getSkipItemByPage(page)
     *        })
     * @param page
     * @return {number}
     */
    getSkipItemByPage: (page = 1, limit = 10) => {
        return ((page - 1) * limit)
    },


    /**
     * Calculator total pages
     * @param totalDocument
     */
    calculatorTotalPages: (totalDocument = 0, limit = 10) => {
        return Math.ceil(totalDocument / limit);
    },

    /**
     * Check if string is mongodb id
     * @param id
     * @return {boolean}
     */
    isMongoId:(id) => {
        return ObjectId.isValid(id);
    },

    /**
     * Convert string to MongoDb ObjectId
     * @param {string} id
     */
    toObjectId: (id) => {
        return mongoose.Types.ObjectId(id);
    },

    /**
     *
     * @param populate
     * @param limit
     */
    optionPaginate: (page = 1, populate, sort, limit = 10) => {
        return {
            lean: true,
            sort: sort,
            populate: populate,
            limit: limit,
            page: page
        }
    },

    /**
     * Convert Array Object to Array Value
     * @param arrayObject
     * @param property
     */
    transferToArrayValue: function (arrayObject, property = '_id') {
        let outputArray = [];
        arrayObject.forEach(object => {
            if (object[property] !== undefined) outputArray.push(object[property]);
        });
        return outputArray;
    },

    /**
     * Return Error instance
     * @param data - of error will create
     *      message: display when log this error
     *      code: access when by error.code
     * @return {object} error instance
     */
    generateError: (data) => {
        let {message, code} = data;
        let error = new Error(message);
        if (code) error.code = code;
        return error;
    },


    /**
     *
     * @param populate
     * [{
     *  path: '',
     *  populate: [{
     *
     *  }]
     *  select: ''
     * }]
     * @param page
     * @param sort
     * {
     *  createdAt: -1
     * }
     * @param limit
     */
    optionPaginate: (page = 1, selectFields, populate, limit = sails.config.paginateLimit) => {
        return {
            select: selectFields,
            page: page,
            limit: limit,
            populate: populate
        }
    },

    /**
     *
     * @param urlImage
     * @return name
     */
    getNameImage: (urlImage) => {
        let pathArr = urlImage.split('/');
        let name = pathArr[pathArr.length - 1];
        return name;
    },

    /**
     *
     * @param page
     * @param arrayLength
     * @param limit
     * @return numberItems
     */
    getNumberItemLoad: (page, arrayLength, limit = 10) => {
        let numberItems = limit*page;
        if (numberItems > arrayLength)
            numberItems = arrayLength;
        return numberItems;
    },

    /**
     * Get minute from Date now
     */
    minuteFromNow: (startTime) => {
        try {
            const end = moment();
            const duration = moment.duration(end.diff(startTime));
            return duration.asMinutes();
        } catch (error) {
            throw error;
        }
    },

    /**
     * Get time Date now
     */
    now: () => {
        try {
            return moment().format();
        } catch (error) {
            throw error;
        }
    },

    /**
     *
     * @param number
     * @returns {boolean}
     */
    isNumber(number) {
        let valid = true;
        if (number === null) {
            valid = false;
        } else if (number === undefined) {
            valid = false;
        } else if (!Number(number)) {
            valid = false;
        }
        return valid;
    },

    /**
     *
     * @param page
     * @return {*}
     */
    getPage: (page) => {
        if (!page || page < 0 || isNaN(page)) page = 1;
        return page;
    }
};
