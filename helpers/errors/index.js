"use strict";

const errors = {
    emailHasExist: {
        message: 'email_exist',
        code: 1,
    },
};

module.exports = {
    error: (errorCode, req) => {
        if (!errorCode || !req)
            throw Error('sails.error require `errorCode` & `req` parameters');

        let error = errors[errorCode];
        if (!error)
            throw new Error('Not Found Error in errors.js');

        return {
            message: req.__(error.message),
            code: error.code
        }
    },

    errors: errors,

    getError: (error) => {
        if (!error)
            throw new Error('Not found error: ' + error);

        return error
    }

};
