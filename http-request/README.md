### HTTP Service

## Implement

```js
const HttpService = require('./HttpService.js');
```

## Usage

```js
const response = await HttpService.post(uri, token);
```

## Files

- HttpService.js (Http Service)
- README.md (note)

## Modules

- [request](https://www.npmjs.com/package/request)

## References
